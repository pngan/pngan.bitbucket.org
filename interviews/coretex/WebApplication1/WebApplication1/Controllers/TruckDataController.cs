﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WebApplication1.Controllers
{
    public class AzureQueueSettings
    {
        public AzureQueueSettings(string connectionString, string queueName)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            if (string.IsNullOrEmpty(queueName))
                throw new ArgumentNullException("queueName");

            this.ConnectionString = connectionString;
            this.QueueName = queueName;
        }

        public string ConnectionString { get; }
        public string QueueName { get; }
    }
    public class AzureQueueSender<T> : IAzureQueueSender<T> where T : class
    {
        public AzureQueueSender(AzureQueueSettings settings)
        {
            m_client = new QueueClient(
                settings.ConnectionString, settings.QueueName);
        }

        public async Task SendAsync(T item, Dictionary<string, object> properties)
        {
            var json = JsonConvert.SerializeObject(item);
            var message = new Message(Encoding.UTF8.GetBytes(json));

            if (properties != null)
            {
                foreach (var prop in properties)
                {
                    message.UserProperties.Add(prop.Key, prop.Value);
                }
            }

            await m_client.SendAsync(message);
        }

        private readonly QueueClient m_client;

    }

    public interface IAzureQueueSender<in T>
    {
        Task SendAsync(T item, Dictionary<string, object> properties);
    }

    [ApiController]
    [Route("[controller]")]
    public class TruckDataController : ControllerBase
    {
        private AzureQueueSettings _settings;

        private readonly IConfiguration m_configuration;
        private readonly ILogger<TruckDataController> _logger;


        public TruckDataController(IConfiguration configuration, ILogger<TruckDataController> logger)
        {

            m_configuration = configuration;
            _settings = new AzureQueueSettings(
                connectionString: m_configuration["ServiceBus:ConnectionString"],
                queueName: m_configuration["ServiceBus:QueueName"]);


            _logger = logger;
        }


        [HttpGet]
        public async Task<IActionResult> GetData()
        {
            return Ok(GetType().Assembly.GetName().Version.ToString());
        }
        [HttpPost]
        public async Task<IActionResult> InsertData()
        {
            var rand = new Random();
            var truckData = new TruckData(
                "freightways", 
                "volvo123",
                rand.Next(),
                rand.Next(),
                rand.Next(),
                rand.Next(),
                rand.Next(),
                rand.Next().ToString()
            );

            IAzureQueueSender<TruckData> sender =
                new AzureQueueSender<TruckData>(_settings);
            Dictionary<string, object> props = new Dictionary<string, object>();
            props["truckId"] = 4565;
            await sender.SendAsync(truckData, props);
            return Ok();
        }
    }

    public class TruckData
    {
        public TruckData(string customerId, string truckId, int longitude, int latitude, int temperature, int pressure, int speed, string driversMessage)
        {
            CustomerId = customerId;
            TruckId = truckId;
            Longitude = longitude;
            Latitude = latitude;
            Temperature = temperature;
            Pressure = pressure;
            Speed = speed;
            DriversMessage = driversMessage;
            TimeStamp = DateTimeOffset.UtcNow;
        }
        public string CustomerId { get; set; }
        public string TruckId { get; set; }
        public int Longitude { get; set; }
        public int Latitude { get; set; }
        public int Temperature { get; set; }
        public int Pressure { get; set; }
        public int Speed { get; set; }
        public string DriversMessage { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
    }

}
