﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.Azure.ServiceBus;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace webapi.Controllers
{
    [Route("/v2/api/[controller]")]
    [ApiController]
    [EnableCors]
    public class TruckDataController : ControllerBase
    {
        private AzureQueueSettings _settings;
        public TruckDataController()
        {
            _settings = new AzureQueueSettings(
                connectionString: "Endpoint=sb://pnservicebus1.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=iNXD5FMBSHswlcSQQG5MOKJ0u5t5812LWF7lgfKgAEw=",
                queueName: "truckdata");
        }

        [HttpGet]
        [Route("ping")]
        public async Task<ActionResult<string>> Ping()
        {
            var response = new PingResponse {Version = GetType().Assembly.GetName().Version.ToString()};
            return Ok(await Task.FromResult(response));
        }


        [HttpGet]
        public async Task<ActionResult<List<TruckEntity>>> Get(string customerId, string truckId)
        {
            TableQuery<TruckEntity> query = new TableQuery<TruckEntity>()
                .Where(TableQuery.GenerateFilterCondition(
                    "PartitionKey", QueryComparisons.Equal,
                    truckId))
                .Take(10);

            CloudStorageAccount cloudStorageAccount =
                CloudStorageAccount.Parse
                    ("DefaultEndpointsProtocol=https;AccountName=pntablestore;AccountKey=ap+hjkhLLu+Bz44SfhbJJp5Qw26QygdpPZKd8TmdlhuD83EWouO3UgIrBpp1VLwwtuHIG4JbKx6PZez5Adcysw==;EndpointSuffix=core.windows.net");
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();
            CloudTable cloudTable = tableClient.GetTableReference(customerId);

            var allItems = new List<TruckEntity>();
            TableContinuationToken tableContinuationToken = null;
            do
            {
                var queryResponse = await cloudTable.ExecuteQuerySegmentedAsync<TruckEntity>(query, tableContinuationToken, null, null);
                tableContinuationToken = queryResponse.ContinuationToken;
                allItems.AddRange(queryResponse.Results);
            }
            while (tableContinuationToken != null && allItems.Count < 10);

            return Ok(allItems);
        }

        [HttpPost]
        public async Task<ActionResult> InsertData([FromBody] TruckData truckData)
        {
            IAzureQueueSender<TruckData> sender =
                new AzureQueueSender<TruckData>(_settings);
            Dictionary<string, object> props = new Dictionary<string, object>();
            props["truckId"] = 4565;
            await sender.SendAsync(truckData, props);
            return Ok();
        }
    }

    public class AzureQueueSettings
    {
        public AzureQueueSettings(string connectionString, string queueName)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            if (string.IsNullOrEmpty(queueName))
                throw new ArgumentNullException("queueName");

            this.ConnectionString = connectionString;
            this.QueueName = queueName;
        }

        public string ConnectionString { get; }
        public string QueueName { get; }
    }
    public class AzureQueueSender<T> : IAzureQueueSender<T> where T : class
    {
        public AzureQueueSender(AzureQueueSettings settings)
        {
            m_client = new QueueClient(
                settings.ConnectionString, settings.QueueName);
        }

        public async Task SendAsync(T item, Dictionary<string, object> properties)
        {
            var json = JsonConvert.SerializeObject(item);
            var message = new Microsoft.Azure.ServiceBus.Message(Encoding.UTF8.GetBytes(json));

            if (properties != null)
            {
                foreach (var prop in properties)
                {
                    message.UserProperties.Add(prop.Key, prop.Value);
                }
            }

            await m_client.SendAsync(message);
        }

        private readonly QueueClient m_client;

    }

    public interface IAzureQueueSender<in T>
    {
        Task SendAsync(T item, Dictionary<string, object> properties);
    }
}

public class TruckData
{
    public TruckData()
    {
        
    }
    public TruckData(string customerId, string truckId, int longitude, int latitude, int temperature, int pressure, int speed, string driversMessage)
    {
        CustomerId = customerId;
        TruckId = truckId;
        Longitude = longitude;
        Latitude = latitude;
        Temperature = temperature;
        Pressure = pressure;
        Speed = speed;
        DriversMessage = driversMessage;
        TimeStamp = DateTimeOffset.UtcNow;
    }
    public string CustomerId { get; set; }
    public string TruckId { get; set; }
    public int Longitude { get; set; }
    public int Latitude { get; set; }
    public int Temperature { get; set; }
    public int Pressure { get; set; }
    public int Speed { get; set; }
    public string DriversMessage { get; set; }
    public DateTimeOffset TimeStamp { get; set; }
}
public class TruckEntity : TableEntity
{
    public TruckEntity() { }

    public string CustomerId { get; set; }
    public string TruckId { get; set; }
    public int Longitude { get; set; }
    public int Latitude { get; set; }
    public int Temperature { get; set; }
    public int Pressure { get; set; }
    public int Speed { get; set; }
    public string DriversMessage { get; set; }
    //public DateTimeOffset TimeStamp { get; set; }
}

public class PingResponse
{ 
    public string Version { get; set; }
}