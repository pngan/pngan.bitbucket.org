using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using Microsoft.Azure.Cosmos;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;


namespace FunctionApp1
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("truckdata", AccessRights.Manage,
                Connection = "SBConnection")]
            string queueItem, TraceWriter log)
        {
            log.Info($"C# ServiceBus queue trigger function about to process message: {queueItem}");

            var truckData = JsonConvert.DeserializeObject<TruckData>(queueItem);

            CloudStorageAccount cloudStorageAccount =
                CloudStorageAccount.Parse
                    ("DefaultEndpointsProtocol=https;AccountName=pntablestore;AccountKey=ap+hjkhLLu+Bz44SfhbJJp5Qw26QygdpPZKd8TmdlhuD83EWouO3UgIrBpp1VLwwtuHIG4JbKx6PZez5Adcysw==;EndpointSuffix=core.windows.net");
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();
            CloudTable cloudTable = tableClient.GetTableReference(truckData.CustomerId);

            TableOperation tableOperation = TableOperation.Insert(truckData);

            cloudTable.Execute(tableOperation);

            //// The Azure Cosmos DB endpoint for running this sample.
            //string EndpointUri = "https://pntablestore.documents.azure.com:443/";
            //// The primary key for the Azure Cosmos account.
            //string PrimaryKey =
            //    "frxTy2ZsclPOpuvplxpZfOc5mX5GIAB9Mkk0yzcDtDtx2NdZ9bHJRM6CmBDMPust5JVEPzWhleGOrO1elsb1IQ==";

            //// The Cosmos client instance
            //CosmosClient cosmosClient = new CosmosClient(EndpointUri, PrimaryKey);

            //// Get database and container
            //string databaseId = "truckdb";
            //Database database = cosmosClient.GetDatabase(databaseId);
            //string containerId = "freightways";
            //Container container = database.GetContainer(containerId);


            log.Info($"C# ServiceBus queue trigger function completed processing message: {queueItem}");

            // The name of the database and container we will create
        }


        public class TruckData : TableEntity
        {
            public TruckData(string customerId, string truckId, int longitude, int latitude, int temperature, int pressure, int speed, string driversMessage, DateTimeOffset timeStamp)
            {
                CustomerId = customerId;
                TruckId = truckId;
                Longitude = longitude;
                Latitude = latitude;
                Temperature = temperature;
                Pressure = pressure;
                Speed = speed;
                DriversMessage = driversMessage;

                PartitionKey = $"{customerId}_{truckId}";
                RowKey = $"{DateTimeOffset.Now.UtcTicks}";
                Timestamp = timeStamp;
            }
            public string CustomerId { get; set; }
            public string TruckId { get; set; }
            public int Longitude { get; set; }
            public int Latitude { get; set; }
            public int Temperature { get; set; }
            public int Pressure { get; set; }
            public int Speed { get; set; }
            public string DriversMessage { get; set; }
        }

    }
}
