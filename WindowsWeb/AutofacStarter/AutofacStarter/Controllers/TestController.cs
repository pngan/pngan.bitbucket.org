﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutofacStarter.Services;

namespace AutofacStarter.API.Controllers
{
    public class TestController : ApiController
    {
        private TestService m_testService;
        public TestController(TestService testService)
        {
            m_testService = testService;
        }

        [HttpGet]
        [Route("ping")]   // run and put /ping at the end of the url
        public async Task<HttpResponseMessage> GetPing()
        {
            var ping = await m_testService.GetPing();
            return Request.CreateResponse(HttpStatusCode.OK, ping);
        }


    }
}
