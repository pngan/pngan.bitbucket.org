﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace OWINAutofacAPIApp.Controllers
{
    [RoutePrefix("api/v1/home")]
    public class HomeController : ApiController
    {
        [HttpGet]
        [Route("get")]
        public IHttpActionResult Get()
        {
            return Ok("Hello, world");
        }
    }
}