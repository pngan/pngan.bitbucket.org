﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Owin;
using OWINAutofacAPIApp.App_Start;

namespace OWINAutofacAPIApp
{
    public class Startup
    {
        
        public void Configuration(IAppBuilder app)
        {
            WebApiConfig.Configure(app);
        }
    }
}