﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin;

namespace OWINAutofacAPIApp.Middleware
{
    public class FirstMiddleware : OwinMiddleware
    {
        private readonly ILogger m_logger;

        public FirstMiddleware(OwinMiddleware next, ILogger logger) : base(next)
        {
            this.m_logger = logger;
        }

        public override async Task Invoke(IOwinContext context)
        {
            this.m_logger.Write("Inside the 'Invoke' method of the '{0}' middleware.", GetType().Name);

            await Next.Invoke(context);
        }
    }
}