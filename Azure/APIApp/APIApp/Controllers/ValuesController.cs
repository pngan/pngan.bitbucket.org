﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;

namespace APIApp.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        [SwaggerOperation("GetAll")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [SwaggerOperation("GetById")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [SwaggerOperation("Create")]
        [SwaggerResponse(HttpStatusCode.Created)]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [SwaggerOperation("Update")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [SwaggerOperation("Delete")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public void Delete(int id)
        {
        }


        [ResponseType(typeof(Address))]
        [HttpPatch]
        [Route("GetUser")]
        public async Task<HttpResponseMessage> GetUser(HttpRequestMessage request)
        {
            var userJson = await request.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<User>(userJson);
            var address = new Address() {Name = user.Name, City = "Gisbourne"};
            return request.CreateResponse(HttpStatusCode.OK, address);
        }
    }

    public class User
    {
        public int Id;
        public string Name;

    }
    public class Address
    {
        public string Name;
        public string City;
    }
}
