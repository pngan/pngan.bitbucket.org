﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace TestClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HttpClient m_client;

        public MainWindow()
        {
            InitializeComponent();
            m_client = new HttpClient();
        }

        private async void ButtonPatch_OnClick(object sender, RoutedEventArgs e)
        {
            var user = new User {Id = 11, Name = "Phil"};
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri("http://localhost:29449/GetUser"),
                Method =  new HttpMethod("PATCH"),
                Content =  new StringContent(JsonConvert.SerializeObject(user))
            };
            var response = await m_client.SendAsync(request);
            var result = await response.Content.ReadAsStringAsync();
            PatchResult.Text = result;
        }
    }


    public class User
    {
        public int Id;
        public string Name;

    }
    public class Address
    {
        public string Name;
        public string City;
    }
}
