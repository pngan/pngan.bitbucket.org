﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace OWINApi.Controllers
{
    public class HomeController : ApiController
    {

        public IHttpActionResult Get()
        {
            return Ok("hello there");
        }
    }
}