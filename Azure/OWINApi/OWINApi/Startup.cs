﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using OWINApi.App_Start;


// OWIN WebApi2 project
// Ref:  https://thompsonhomero.wordpress.com/2015/01/21/creating-a-clean-web-api-2-project-with-external-authentication/


namespace OWINApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            WebApiConfig.Configure(app);
        }
    }
}