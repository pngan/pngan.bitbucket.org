using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace FindPrimes
{
    public static class FindPrimes
    {
        [FunctionName("HttpTriggerWithParametersCSharp")]

        public static HttpResponseMessage Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = "HttpTriggerCSharp/value/{num}")]HttpRequestMessage req, string num, TraceWriter log)
        {
            int value;

            if (int.TryParse(num, out value) == false)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Please pass an integer on the query string or in the request body, e.g. primes?number=100");
            }


            var array = new List<int>(value + 1);
            array.AddRange(Enumerable.Range(0, value));
            for (var i = 2; i < value / 2; i++)
            {
                for (int y = i * 2; y < value; y += i)
                {
                    array[y] = 0;
                }
            }

            string s = string.Join(",", array.Where(x => x > 0));



            return num == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a num on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, $"Primes of {value} " + s);
        }
    }
}