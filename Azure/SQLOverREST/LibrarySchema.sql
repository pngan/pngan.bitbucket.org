create table Books (
	BookId int IDENTITY(1,1) PRIMARY KEY,
	Title varchar(255),
	Publication DATE
	)

create table Users (
	UserId int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(255)
)

create table Loans (
	LoanId int IDENTITY(1,1) PRIMARY KEY,
	FK_BookId int FOREIGN KEY REFERENCES Books(BookId),
	FK_UserId  int FOREIGN KEY REFERENCES Users(UserId),
	DateDue DATE,
	DateReturned DATE
)
	
drop table Loans
