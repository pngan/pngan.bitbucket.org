#include <Servo.h>

Servo servo;

void setup() {
  servo.attach(8);
  servo.write(0);
  delay(2000);

}

void loop() {
  int angle;
  for(angle=10; angle<180; angle++)
  {
    servo.write(angle);
    delay(15);
  }

  for(angle=180; angle > 10; angle--)
  {
    servo.write(angle);
    delay(15);
  }
}
