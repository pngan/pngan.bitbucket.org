int piezo = 8;
int duration = 250;
int notes[] = {261,294,329,349,392,440,493,523,587,659,698,783,880};

int w = 1000;
int h = 500;




void setup()
{
  pinMode(piezo, OUTPUT);

  c(h);
  c(h);
  g(h);
  g(h);
  a(h);
  a(h);
  g(w);

  f(h);
  f(h);
  e(h);
  e(h);
  d(h);
  d(h);
  c(w);

  g(h);
  g(h);
  f(h);
  f(h);
  e(h);
  e(h);
  d(w);

  g(h);
  g(h);
  f(h);
  f(h);
  e(h);
  e(h);
  d(w);
  
  c(h);
  c(h);
  g(h);
  g(h);
  a(h);
  a(h);
  g(w);

  f(h);
  f(h);
  e(h);
  e(h);
  d(h);
  d(h);
  c(w+w);
}

void loop()
{

}




void c(int duration) { tone(piezo,notes[0],duration); delay(duration); }
void d(int duration) { tone(piezo,notes[1],duration); delay(duration); }
void e(int duration) { tone(piezo,notes[2],duration); delay(duration); }
void f(int duration) { tone(piezo,notes[3],duration); delay(duration); }
void g(int duration) { tone(piezo,notes[4],duration); delay(duration); }
void a(int duration) { tone(piezo,notes[5],duration); delay(duration); }
void b(int duration) { tone(piezo,notes[6],duration); delay(duration); }
void cc(int duration) { tone(piezo,notes[7],duration); delay(duration); }
void dd(int duration) { tone(piezo,notes[8],duration); delay(duration); }
void ee(int duration) { tone(piezo,notes[9],duration); delay(duration); }
void ff(int duration) { tone(piezo,notes[10],duration); delay(duration); }
void gg(int duration) { tone(piezo,notes[11],duration); delay(duration); }
void aa(int duration) { tone(piezo,notes[12],duration); delay(duration); }

