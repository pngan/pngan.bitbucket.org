#include <ESP8266WiFi.h>
#include <async_config.h>
#include <AsyncPrinter.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncTCPbuffer.h>
#include <SyncClient.h>
#include <tcp_axtls.h>
#include <AsyncEventSource.h>
#include <AsyncJson.h>
#include <AsyncWebSocket.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFSEditor.h>
#include <StringArray.h>
#include <WebAuthentication.h>
#include <WebHandlerImpl.h>
#include <WebResponseImpl.h>


#include "secrets.h"


// Load DHT11 library using Library Manager
#include <DHT.h>


#define DHT1PIN 4 // Digital pin connected to the DHT sensor in Attic
#define DHT2PIN 5 // Digital pin connected to the DHT sensor in House
#define DHTTYPE DHT11 // DHT 11

DHT dht1(DHT1PIN, DHTTYPE);
DHT dht2(DHT2PIN, DHTTYPE);

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0; // will store last time DHT was updated

// Updates DHT readings every 16 seconds (ThingSpeak free limits updates to every 15 seconds)
const long interval = 60000;

// current temperature & humidity, updated in loop()
float t1 = 0.0;
float h1 = 0.0;
float t2 = 0.0;
float h2 = 0.0;

AsyncWebServer server(80);
 
 

enum State { STATE_OFF, STATE_ON };
const float ON_HUMIDITY_THRESHOLD = 93.0;
const float OFF_HUMIDITY_THRESHOLD = 90.0;
State _state = STATE_OFF;

bool UploadDataToThingSpeak(float h1, float t1, float h2, float t2)
{
  const char *resource = "/update?api_key=";

  // Thing Speak API server
  const char *thingSpeakApi = "api.thingspeak.com";
  const int thingSpeakPort = 80;

  WiFiClient client;

  if (!client.connect(thingSpeakApi, thingSpeakPort))
  {
    return false;
  }

  // compose ThindSpeak update data url
  String url = resource;
  url = url + SECRET_WRITE_APIKEY;
  url = url + "&field1=";
  url = url + String(h1);
  url = url + "&field2=";
  url = url + String(t1);
  url = url + "&field3=";
  url = url + String(h2);
  url = url + "&field4=";
  url = url + String(t2);
  url = url + "&field5=";
  url = url + String(_state);
  url = url + "\r\n";

  Serial.println(url);
  // This will send the request to the ThingSpeak
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + thingSpeakApi + "\r\n" +
               "Connection: close\r\n\r\n");

  return true;
}

void setup()
{
  Serial.begin(115200);
  
  dht1.begin();
  dht2.begin();
  WiFi.begin(SECRET_SSID, SECRET_PASS);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
 
  Serial.println(WiFi.localIP());
 
  server.on("/data", HTTP_GET, [](AsyncWebServerRequest *request)
  {
    String response = "[{t1: " + String(t1) + ", h1: " + String(h1) + "}, {t2: " + String(t2) + ", h2: " + String(h2)+ "}]";
    request->send(200, "text/plain", response);
  });
 
  server.begin();

  
  pinMode(12, OUTPUT);
  OffAction();
}

void OnAction()
{
   digitalWrite(12, HIGH); 
}
 
void OffAction()
{
   digitalWrite(12, LOW); 
}


const float humidityHysteresis = 3.0;
const float temperatureHysteresis = 2.0;
const float maxTemperature = 26.0;

void SchmittTrigger(void (*onAction)(), void (*offAction)())
{
  if (t1 >= maxTemperature)
  {
    (*offAction)();
    _state = STATE_OFF;
  }
  else if (_state == STATE_ON)
  {
    if ((t1 <= t2) || (h1 >= h2))
    {
      (*offAction)();
      _state = STATE_OFF;
    }
  }
  else
  {
    if (t1 >= (t2 + temperatureHysteresis) && h1 <= (h2 - humidityHysteresis))
    {
      (*onAction)();
      _state = STATE_ON;
    }
  }
}

const float tempAdjustment = 0.5;
const float humdityAdjustment = 9.0;

void loop()
{
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval)
    {
        // save the last time you updated the DHT values
        previousMillis = currentMillis;
        // Read temperature as Celsius (the default)
        float newT1 = dht1.readTemperature();
        float newT2 = dht2.readTemperature();
        // if temperature read failed, don't change t value
        if (isnan(newT1) )
        {
            Serial.println("Failed to read from DHT1 sensor!");
        }
        else if (isnan(newT2) )
        {
            Serial.println("Failed to read from DHT2 sensor!");
        }
        else
        {
            t1 = newT1 + tempAdjustment;
            t2 = newT2;
            Serial.print("Temperature: ");
            Serial.print(t1);
            Serial.print(", ");
            Serial.println(t2);
        }
        // Read Humidity
        float newH1 = dht1.readHumidity();
        float newH2 = dht2.readHumidity();
        // if humidity read failed, don't change h value
        if (isnan(newH1))
        {
            Serial.println("Failed to read from DHT1 sensor!");
        }
        else if (isnan(newH2))
        {
            Serial.println("Failed to read from DHT2 sensor!");
        }
        else
        {
            h1 = newH1 + humdityAdjustment;
            h2 = newH2;
            Serial.print("Humidity   : ");
            Serial.print(h1);
            Serial.print(", ");
            Serial.println(h2);
        }

        Serial.println("begin to upload data to ThingSpeak.");
        //update ThingSpeak channel with new values
        bool isUploaded = UploadDataToThingSpeak(h1, t1, h2, t2);

        if (isUploaded)
        {
          Serial.println(" data uploaded!");
        }
        else
        {
          Serial.println(" data upload failed!");
        }

        SchmittTrigger(OnAction, OffAction);
        Serial.print("state      : ");
        Serial.println(_state);
    }

}
