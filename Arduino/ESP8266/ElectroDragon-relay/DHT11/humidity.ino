// Outputs the humidity and temperature from DHT11 sensor connected to 
// GPIO-5 pin. 
// Requires DHT11 and Adafruit Unified sensor libary to be loaded
// using the IDE Library Manager.


// Load the Adafruit Unified Sensor Library using Library Manager
#include <Adafruit_Sensor.h>

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

// Load DHT11 library using Library Manager
#include <DHT.h>

#define DHTPIN 5 // Digital pin connected to the DHT sensor

// Uncomment the type of sensor in use:
#define DHTTYPE DHT11 // DHT 11

DHT dht(DHTPIN, DHTTYPE);

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0; // will store last time DHT was updated

// Updates DHT readings every 10 seconds
const long interval = 10000;

// current temperature & humidity, updated in loop()
float t = 0.0;
float h = 0.0;

void setup()
{
    // Serial port for debugging purposes
    Serial.begin(115200);
    dht.begin();
}

void loop()
{
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval)
    {
        // save the last time you updated the DHT values
        previousMillis = currentMillis;
        // Read temperature as Celsius (the default)
        float newT = dht.readTemperature();
        // Read temperature as Fahrenheit (isFahrenheit = true)
        //float newT = dht.readTemperature(true);
        // if temperature read failed, don't change t value
        if (isnan(newT))
        {
            Serial.println("Failed to read from DHT sensor!");
        }
        else
        {
            t = newT;
            Serial.print("Temperature: ");
            Serial.println(t);
        }
        // Read Humidity
        float newH = dht.readHumidity();
        // if humidity read failed, don't change h value
        if (isnan(newH))
        {
            Serial.println("Failed to read from DHT sensor!");
        }
        else
        {
            h = newH;
            Serial.print("Humidity   : ");
            Serial.println(h);
        }
    }
}