import time
import machine
led = machine.Pin(2, machine.Pin.OUT)
led.off()
led.on()
while True:
    led.off()
    time.sleep(0.5)
    led.on()
    time.sleep(0.5)