int redLedPin = 9;
int greenLedPin = 10;
int blueLedPin = 11;
int lightLevel;

void setup() {
	Serial.begin(38400);
	pinMode(redLedPin, OUTPUT);
	pinMode(greenLedPin, OUTPUT);
	pinMode(blueLedPin, OUTPUT);
}

void loop() {
	lightLevel = analogRead(A0);

	//Serial.print("Light level: ");
	// Serial.println(lightLevel, DEC);

	if (lightLevel > 70)
		displayWhite();
	else if (lightLevel > 50)
		displayPink();
	else if (lightLevel > 30)
		displayPurple();
	else
		displayBlue();

	delay(100);
}

void displayLedColor(int red, int green, int blue)
{
	analogWrite(redLedPin, blue);
	analogWrite(greenLedPin, green);
	analogWrite(blueLedPin, red);
}

void displayWhite()
{
	displayLedColor(255, 255, 255);
}


void displayPink()
{
	displayLedColor(255, 70, 70);
}


void displayPurple()
{
	displayLedColor(255, 0, 255);
}


void displayBlue()
{
	displayLedColor(0, 0, 255);
}


