int redLedPin = 9;
int greenLedPin = 10;
int blueLedPin = 11;
int lightLevel;
#define echoPin 12 // Echo Pin
#define trigPin 13 // Trigger Pin

int maximumRange = 10; // Maximum distance (cm) for car is present
int minimumRange = 0; // Minimum distance (cm) for car is present
long duration, distance; // Duration used to calculate distance


unsigned long maybe_car_start;
unsigned long maybe_nocar_start;
unsigned long orange_light_start;

enum State{ 
  no_car,
  maybe_car,
  car,
  maybe_no_car,
  orange_light
};

State state;




void setup() {
	Serial.begin(115200);

  // traffic light pins
	pinMode(redLedPin, OUTPUT);
	pinMode(greenLedPin, OUTPUT);
	pinMode(blueLedPin, OUTPUT);

  // distance sensor pins
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  // Initialize state machine
 state = no_car;
}

void loop() {
 
readDistance();
 Serial.println(distance);
 
  bool isCarPresent = distance>minimumRange && distance < maximumRange;
  switch (state) {
    case no_car:
      if (isCarPresent)
      {
        state = maybe_car;
        maybe_car_start = micros();
      }
      displayRed();
      break;
    case maybe_car:
      if (isCarPresent)
      {
        if ((micros() - maybe_car_start) > 2000000)
        {
          state = car;
        }
      }
      else
      {
        state = no_car;
      }
      displayRed();
      break;
    case car:
      if (isCarPresent)
      {}
      else
      {
        state = maybe_no_car;
        maybe_nocar_start = micros();
      }
      displayGreen();
  
      break;
    case maybe_no_car:
      if (isCarPresent)
      {
          state = car;
      }
      else
      {
        if ((micros() - maybe_nocar_start) > 3000000)
        {
          state = orange_light;
          orange_light_start = micros();        }
      }
      displayGreen();
      break;
      
    case orange_light:
      if ((micros() - orange_light_start) > 2000000)
      {
        state = no_car;
      }
      displayOrange();
      break;
    default: 
      // statements
    break;
  }
 delay(50);
}

void displayLedColor(int red, int green, int blue)
{
	analogWrite(redLedPin, blue);
	analogWrite(greenLedPin, green);
	analogWrite(blueLedPin, red);
}

void displayRed()
{
  Serial.print("Red");
	displayLedColor(255, 0, 0);
}

void displayOrange()
{

  Serial.print("Orange");
  displayLedColor(255, 50, 0);
}

void displayGreen()
{
  
  Serial.print("Green");
	displayLedColor(0, 255, 0);
}

int readDistance()
{
 digitalWrite(trigPin, LOW); 
 delayMicroseconds(2); 

 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10); 
 
 digitalWrite(trigPin, LOW);
 duration = pulseIn(echoPin, HIGH);
 
 distance = duration/58.2; // converts distance reading to cm
}


