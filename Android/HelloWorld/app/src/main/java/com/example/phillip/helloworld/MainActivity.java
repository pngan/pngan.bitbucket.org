package com.example.phillip.helloworld;
import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Phillip on 2/02/2015.
 */
public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

