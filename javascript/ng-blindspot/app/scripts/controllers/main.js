'use strict';

angular.module('ngBlindspotApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  })
    .controller('MyCtrl1', [function($scope) {
        var stage = new Kinetic.Stage({
            container: 'container',
            width: 768,
            height: 200
        });
        var layer = new Kinetic.Layer();

        var plusHalfSize = 10;
        var plusX = stage.width() * 0.8;
        var plusY = stage.height()/2;

        var circleX = stage.width()*0.4;
        var circleY = stage.height()/2;
        var circle = new Kinetic.Circle({
            x: circleX,
            y: circleY,
            radius: 10,
            fill: 'red'
        });

        var plusH = new Kinetic.Line({
            points: [plusX-plusHalfSize, plusY,plusX+plusHalfSize, plusY  ],
            stroke: 'black',
            strokeWidth: 8,
            lineCap: 'square'
        });
        var plusY = new Kinetic.Line({
            points: [plusX, plusY+plusHalfSize, plusX, plusY-plusHalfSize  ],
            stroke: 'black',
            strokeWidth: 8,
            lineCap: 'square'
        });

        layer.add(plusH);
        layer.add(plusY);
        layer.add(circle);
        stage.add(layer);

        $("#container").click(function(e) {
            if(e.which) {
                var amplitude = 200;
                var period = 10000;
                // in ms

                var anim = new Kinetic.Animation(function(frame) {
                    circle.setX(amplitude * Math.sin(frame.time * 2 * Math.PI / period) + circleX);
                }, layer);

                anim.start();
            }
        });

        var rangeX = 200;
        var deltaX = rangeX / 11;
        for ( var pos = -5; i <= 5; i++)
        {
            circle.setX( (pos * deltaX) + circleX);

        }


    }])
    .controller('MyCtrl2', [function() {
        var stage = new Kinetic.Stage({
            container: 'container',
            width: 578,
            height: 200
        });
        var layer = new Kinetic.Layer();

        var hexagon = new Kinetic.RegularPolygon({
            x: stage.width()/2,
            y: stage.height()/2,
            sides: 6,
            radius: 50,
            fill: 'blue',
            stroke: 'green',
            strokeWidth: 9
        });

        layer.add(hexagon);
        stage.add(layer);

        var amplitude = 150;
        var period = 2000;
        // in ms
        var centerX = stage.width()/2;

        var anim = new Kinetic.Animation(function(frame) {
            hexagon.setX(amplitude * Math.sin(frame.time * 2 * Math.PI / period) + centerX);
        }, layer);

        anim.start();

    }])
    .controller('MyCtrl3', [function() {
    }]);

