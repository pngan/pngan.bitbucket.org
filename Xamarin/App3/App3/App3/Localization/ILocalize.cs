﻿using System.Globalization;

namespace App3
{
	public interface ILocalize
	{
		CultureInfo GetCurrentCultureInfo ();

		void SetLocale ();
	}
}

