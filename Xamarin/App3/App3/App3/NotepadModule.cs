﻿using Autofac;

namespace App3
{
    public class NotepadModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<NotepadViewModel>().As<INotepadViewModel>().SingleInstance();
            builder.RegisterType<NotepadView>().As<NotepadView>().SingleInstance();
        }
    }
}
