﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using App3.Annotations;
using Xamarin.Forms;

namespace App3
{
    public class NoteItem
    {
        public NoteItem(string text, INotepadViewModel context)
        {
            Text = text;
            Timestamp = DateTime.Now.ToString(CultureInfo.CurrentUICulture);
            Context = context;
        }
        public string Text { get; private set; }    
        public string Timestamp { get; private set; }
        public INotepadViewModel Context { get; private set; }
    }

    public class NotepadViewModel : INotifyPropertyChanged, INotepadViewModel
    {
        public NotepadViewModel()
        {
            AddNoteCommand = new Command(AddNoteAction);
            DeleteNoteCommand = new Command(DeleteNoteAction);
        }

        private void DeleteNoteAction(object obj)
        {
            
        }

        private void AddNoteAction(object obj)
        {
            m_noteCollection.Add(new NoteItem(InputText, this));
        }

        private string m_inputText;
        private readonly ObservableCollection<NoteItem> m_noteCollection = new ObservableCollection<NoteItem>();
        private ICommand m_deleteNoteCommand;

        public string InputText
        {
            get { return m_inputText; }
            set
            {
                m_inputText = value; 
                OnPropertyChanged();
            }
        }

        public IEnumerable<NoteItem> NoteCollection
        {
            get { return m_noteCollection; }
        }

        public ICommand AddNoteCommand { get; private set; }
        public ICommand DeleteNoteCommand { get; private set; }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public interface INotepadViewModel
    {
        string InputText { get; set; }
        IEnumerable<NoteItem> NoteCollection { get; }
        ICommand AddNoteCommand { get; }
        ICommand DeleteNoteCommand { get; }
    }
}
