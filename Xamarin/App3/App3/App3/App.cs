﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Xamarin.Forms;

namespace App3
{
    public class App : Application
    {
        public App()
        {

        }
        
        protected override void OnStart()
        {  
            var builder = new ContainerBuilder();
            builder.RegisterModule<NotepadModule>();
            var container = builder.Build();

            var page = container.Resolve<NotepadView>();
            MainPage = new NavigationPage(page);
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
