﻿namespace App3
{
    public partial class NotepadView
    {
        public NotepadView(INotepadViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = viewModel;
        }
    }
}
