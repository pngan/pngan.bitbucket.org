-------------- Create Schema --------------
create table A (`id` int);
insert into A  (`id` )
values (1),(2),(3),(4);

create table B (`id` int);
insert into B  (`id` )
values (3),(4),(5),(6);

select * from A;
select * from B;

-------------- Test Joins ---------------
select * from A join B;
select * from A cross join B;
select * from A inner join B;
select * from A,B;


select * from A join B on A.id = B.id;
select * from A join B on A.id < 3;

select * from A left join B on A.id = B.id;

select * from A left outer join B on A.id = B.id;