﻿using System;

namespace ConsolePOC1.Domain
{
    public class ConversationManagerEventArgs : EventArgs
    {
        public ConversationManagerEventArgs(IConversation conversation)
        {
            Conversation = conversation;
        }

        public IConversation Conversation { get; private set; }
    }
}