﻿namespace ConsolePOC1.Domain
{
    public enum ConversationState
    {
        Inactive, Active, Parked, Terminated, Invalid
    }
}