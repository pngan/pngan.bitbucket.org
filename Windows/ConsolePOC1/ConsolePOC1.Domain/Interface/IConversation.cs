﻿using System.Collections.Generic;

namespace ConsolePOC1.Domain
{
    public interface IConversation
    {
        IParticipant AddParticipant(IContact contact);
        void RemoveParticipant(IParticipant participant);
        IList<IParticipant> Participants { get; }
        ConversationState ConversationState { get; }

        void Forward(IContact contact);
        void Forward(IContactEndpoint endPoint);
    }
}