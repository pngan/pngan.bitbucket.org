namespace ConsolePOC1.Domain
{
    public interface IContactEndpoint
    {
        string DisplayName { get; }
        ContactEndpointType Type { get; }
        string Uri { get; }
    }
}