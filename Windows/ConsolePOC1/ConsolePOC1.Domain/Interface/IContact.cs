﻿namespace ConsolePOC1.Domain
{
    public interface IContact
    {
        IContactEndpoint CreateEndpoint(string telephoneUri);
    }
}