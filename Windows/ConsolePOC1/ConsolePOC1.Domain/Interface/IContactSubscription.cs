namespace ConsolePOC1.Domain
{
    public interface IContactSubscription
    {
        void AddContact(IContact contact);
        void RemoveContact(IContact contact);
    }
}