﻿using System.Collections.Generic;

namespace ConsolePOC1.Domain
{
    public interface IContactManager
    {
        IContact GetContactByUri(string contactUri);
        IList<IContact> Search(string searchString);

        IContactSubscription CreateSubscription();
    }
}