﻿using System;
using System.Collections.Generic;

namespace ConsolePOC1.Domain
{
    public interface IConversationManager
    {
        IConversation AddConversation();
        IList<IConversation> Conversations { get; }

        event EventHandler<ConversationManagerEventArgs> ConversationAdded;
        event EventHandler<ConversationManagerEventArgs> ConversationRemoved;

    }
}