﻿namespace ConsolePOC1.Domain
{
    public enum ContactEndpointType
    {
        SipAddress,
        VoiceMail
    }
}