﻿using NUnit.Framework;
using System.Collections.Generic;
using ConsolePOC1.Domain;
using Moq;

namespace ConsolePOC1.Tests
{
    [TestFixture]
    public class ServiceTests
    {
        private IService CreateService(int numberOfActiveConversations = 0, int numberOfInactiveConversations = 0, IConversationManager conversationManager = null)
        {
            var mockConversationManager = new Mock<IConversationManager>();

            if (conversationManager == null)
            {
                conversationManager = mockConversationManager.Object;
                var activeConversations = new List<IConversation>();
                for (int i = 0; i < numberOfActiveConversations; i++)
                {
                    activeConversations.Add(Mock.Of<IConversation>(c => c.ConversationState == ConversationState.Active));
                }
                mockConversationManager.Setup(m => m.Conversations).Returns(activeConversations);
            }

            return new Service(conversationManager ?? mockConversationManager.Object);
        }

        [Test]
        public void given_an_initial_active_conversations_should_have_active_conversations(
            [Values(0,1)] int numberOfActiveConversations,
            [Values(0,1)] int numberOfInactiveConversations)
        {
            var service = CreateService(numberOfActiveConversations, numberOfInactiveConversations);
            service.Initialize();
            Assume.That(service.ActiveConversations, Is.Not.Null);

            Assert.That(service.ActiveConversations.Count, Is.EqualTo(numberOfActiveConversations));
        }
        [Test]
        public void when_an_active_conversation_is_added_should_have_active_conversation_and_a_corresponding_event()
        {
            var mockConversationManager = new Mock<IConversationManager>();
            mockConversationManager.Setup(cm => cm.Conversations).Returns(new List<IConversation>());

            var service = CreateService(conversationManager: mockConversationManager.Object);
            service.Initialize();
            Assume.That(service.ActiveConversations, Is.Not.Null);
            Assume.That(service.ActiveConversations.Count, Is.EqualTo(0));
            bool conversationAdded = false;
            service.ActiveConversationAdded += (o, args) => conversationAdded = true; ;

            // when_an_active_conversation_is_added
            mockConversationManager.Raise(cm=>cm.ConversationAdded += null, new ConversationManagerEventArgs(Mock.Of<IConversation>(c=>c.ConversationState==ConversationState.Active)));

            // should_have_active_conversation_and_a_corresponding_event
            Assert.That(service.ActiveConversations.Count, Is.EqualTo(1));
            Assert.That(conversationAdded, Is.True);
        }

        [Test]
        public void when_an_inactive_conversation_is_added_should_not_have_active_conversation_nor_a_corresponding_event()
        {
            var mockConversationManager = new Mock<IConversationManager>();
            mockConversationManager.Setup(cm => cm.Conversations).Returns(new List<IConversation>());

            var service = CreateService(conversationManager: mockConversationManager.Object);
            service.Initialize();
            Assume.That(service.ActiveConversations, Is.Not.Null);
            Assume.That(service.ActiveConversations.Count, Is.EqualTo(0));
            bool conversationAdded = false;
            service.ActiveConversationAdded += (o, args) => conversationAdded = true; ;

            // when_an_inactive_conversation_is_added
            mockConversationManager.Raise(cm => cm.ConversationAdded += null, new ConversationManagerEventArgs(Mock.Of<IConversation>(c => c.ConversationState == ConversationState.Inactive)));

            // should_not_have_active_conversation_nor_a_corresponding_event
            Assert.That(service.ActiveConversations.Count, Is.EqualTo(0));
            Assert.That(conversationAdded, Is.False);
        }
    }
}