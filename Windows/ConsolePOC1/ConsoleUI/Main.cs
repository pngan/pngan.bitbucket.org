﻿using Autofac;
using JetBrains.Annotations;

namespace ConsoleUI
{
    public class Main : IStartable
    {
        [NotNull]
        private readonly ILifetimeScope m_lifetimeScope;

        public Main(ILifetimeScope lifetimeScope)
        {
            m_lifetimeScope = lifetimeScope;
        }

        public void Start()
        {
            using (var mainScope = m_lifetimeScope.BeginLifetimeScope())
            {
                var window = mainScope.Resolve<MainWindow>();
                window?.Show();
            }
        }
    }
}
