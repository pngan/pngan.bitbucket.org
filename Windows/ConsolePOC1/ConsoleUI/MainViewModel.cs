﻿namespace ConsoleUI
{
    public interface IMainViewModel { }
    public class MainViewModel : ViewModelBase, IMainViewModel
    {

        public string Greeting { get { return "Greetings from the ViewModel!"; } }

        public MainViewModel()
        {
            
        }
    }
}