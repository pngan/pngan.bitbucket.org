﻿using System.Reflection;
using System.Windows;
using Autofac;
using ConsoleUI.Logging;
using Serilog;

namespace ConsoleUI
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IContainer m_container;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            var builder = new ContainerBuilder();
            builder.Register(c => SerilogConfig.CreateLogger())
               .As<ILogger>()
               .SingleInstance();
            builder.RegisterModule<SerilogLoggingModule>();
            builder.RegisterAssemblyTypes(thisAssembly).AsImplementedInterfaces();

            builder.RegisterAssemblyModules(GetSatelliteModules());


            builder.RegisterType<MainWindow>();
            builder.RegisterType<MainViewModel>();

            m_container = builder.Build();
        }

        private Assembly[] GetSatelliteModules()
        {
            AssemblyName serviceAssemblyName = AssemblyName.GetAssemblyName("ConsolePOC1.Service.dll");
            Assembly serviceAssembly = Assembly.Load(serviceAssemblyName);

            AssemblyName domainAssemblyName = AssemblyName.GetAssemblyName("ConsolePOC1.Domain.dll");
            Assembly domainAssembly = Assembly.Load(domainAssemblyName);

            return new[]{ serviceAssembly, domainAssembly};
        }
    }
}