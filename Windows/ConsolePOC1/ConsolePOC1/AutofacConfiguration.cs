﻿using Autofac;

namespace ConsolePOC1
{
    public class TeamcityBuildSourceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Service>().As<IService>().SingleInstance();
        }
    }
}
