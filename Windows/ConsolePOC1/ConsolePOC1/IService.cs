﻿using System;
using System.Collections.Generic;
using ConsolePOC1.Domain;
using JetBrains.Annotations;


namespace ConsolePOC1
{
    public interface IService
    {
        [NotNull]
        List<IConversation> ActiveConversations { get; }
        void Initialize();

        event EventHandler<ConversationManagerEventArgs> ActiveConversationAdded;
    }
}