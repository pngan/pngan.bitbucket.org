﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsolePOC1.Domain;
using JetBrains.Annotations;

namespace ConsolePOC1
{
    public class Service : IService
    {
        public Service(IConversationManager conversationManager)
        {
            m_conversationManager = conversationManager;
        }


        [NotNull]
        private List<IConversation> m_activeConversations = new List<IConversation>();

        private IConversationManager m_conversationManager;

        [NotNull]
        public List<IConversation> ActiveConversations
        {
            get { return m_activeConversations; }
        }

        public void Initialize()
        {
            m_activeConversations =
                m_conversationManager.Conversations.Where(c => c.ConversationState == ConversationState.Active).ToList();

            m_conversationManager.ConversationAdded += OnConversationAdded;

        }

        private void OnConversationAdded(object sender, ConversationManagerEventArgs e)
        {
            var conversation = e.Conversation;
            if (conversation == null)
                return;
            switch (conversation.ConversationState)
            {
                case ConversationState.Inactive:
                    break;
                case ConversationState.Active:
                    m_activeConversations.Add(conversation);
                    InvokeActiveConversationAdded(new ConversationManagerEventArgs(conversation));
                    break;
                case ConversationState.Parked:
                    break;
                case ConversationState.Terminated:
                    break;
                case ConversationState.Invalid:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        public event EventHandler<ConversationManagerEventArgs> ActiveConversationAdded;

        protected virtual void InvokeActiveConversationAdded(ConversationManagerEventArgs e)
        {
            ActiveConversationAdded?.Invoke(this, e);
        }
    }
}