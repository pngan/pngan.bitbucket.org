﻿using System;

namespace Model
{
    public interface IMainModel
    {
        void Start();
        void CreateKeys(string password);

        event EventHandler<LicenseArgs> OnLicenseChanged;
        IObservable<LicenseArgs> LicenseObservable { get;  }

        event EventHandler<KeysArgs> OnKeysChanged;
        IObservable<KeysArgs> KeysObservable { get;  }
        
        void CreateLicense(string customer, string email);
        void LoadLicense();
        bool HasKeys();
    }
}