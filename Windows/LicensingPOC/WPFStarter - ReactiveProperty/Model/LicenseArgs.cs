using System;

namespace Model
{
    public class LicenseArgs : EventArgs
    {
        public LicenseArgs(string customer, string email, string errors)
        {
            Customer = customer;
            Email = email;
            Errors = errors;
        }

        public string Customer { get; }
        public string Email { get; }
        public string Errors { get; }
    }


    public class KeysArgs : EventArgs
    {
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }

        public KeysArgs(string publicKey, string privateKey)
        {
            PublicKey = publicKey;
            PrivateKey = privateKey;
        }
    }
}