﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using Microsoft.Win32;
using Portable.Licensing;
using Portable.Licensing.Validation;

namespace Model
{
    public class MainModel : IMainModel
    {
        private string m_privateKey;
        private string m_publicKey;
        private string m_password;

        public void Start()
        {
            // Comment below to regenerate the public and private keys
            m_password = "Enghouse Interactive Password";
            m_publicKey = "MIIBKjCB4wYHKoZIzj0CATCB1wIBATAsBgcqhkjOPQEBAiEA/////wAAAAEAAAAAAAAAAAAAAAD///////////////8wWwQg/////wAAAAEAAAAAAAAAAAAAAAD///////////////wEIFrGNdiqOpPns+u9VXaYhrxlHQawzFOw9jvOPD4n0mBLAxUAxJ02CIbnBJNqZnjhE50mt4GffpAEIQNrF9Hy4SxCR/i85uVjpEDydwN9gS3rM6D0oTlF2JjClgIhAP////8AAAAA//////////+85vqtpxeehPO5ysL8YyVRAgEBA0IABOjLORYFcqcbNF3/9YRkUuJrNI3XEUxHol2u+WTEyVsA7PikVCr9EnWvXAxZc611OYNgPALFBC98sqyvBtBb/vU=";
            m_privateKey = "MIICITAjBgoqhkiG9w0BDAEDMBUEEKsJOOeQH0jWc+lRLl/JA+sCAQoEggH4ovC1ZnrTQDHvbY6J0ixjEWLOHRRw930rkKACY6NlN3PiZsKLXWFcQYRXHTvQT7kEFiEttY4SC+uExNUCyHK5ILKj12Wv83zEHMlWgA5wOtiCsWBOj1HfB14xAcuZ0d2/oNYUjPBjGeSpb3OkWyZ0032SUc0YqtpbbXLSGhiN9xIdlqbwexxU+zS5VaPl0Vd2R1tRPS1Fx+qdYEqgErfRtnUw8X53GJlxBUs4PCJCBrsv6nM7LL5yWdgkT74PCP0zH2kWLCReWdId6kZ6S4XX8oP0mHHpsgRScT/4gi70oTNc83+xLPbvFsvbSEswlHqJKrQmUWNPC9pEd8jNRO9taNGfCisbu0Po4NzB6SsFAnYf1VZ/zZdKgjF1bhlNm7U9O8JqxcPUjX/BIIK+tngAoAM/dnFKRnxa7QZm08i6GYO/iR6UDstG+cjh1tqmzguOrEdBEJQf4GVesLI0UV0mPk7W6JUPB+X20cnq3USjTKLOIwoK00P0EArj8wZ/3meS9xFx9AaFyNxXmq0bEqgYYkjl01G+qlM3DxqsmgyrKgwgfMyNhHLiZyJsjxW7pvckvdygikI/qvL20m+qsJISBCN7LzLjqg+mKtWndqX3cX5TQ4xQaR6Qu17RQs6ZePc1KtfgK8nf2mTa+IL/3mukq/FiQ1tPKY1E";

            OnKeysChanged?.Invoke(this, new KeysArgs(m_publicKey, m_privateKey));
        }

        public void CreateKeys(string password)
        {
            m_password = password;
            var keyGenerator = Portable.Licensing.Security.Cryptography.KeyGenerator.Create();
            var keyPair = keyGenerator.GenerateKeyPair();
            m_privateKey = keyPair.ToEncryptedPrivateKeyString(password);
            m_publicKey = keyPair.ToPublicKeyString();

            OnKeysChanged?.Invoke(this, new KeysArgs(m_publicKey, m_privateKey));
        }

        public event EventHandler<LicenseArgs> OnLicenseChanged;
            
        public IObservable<LicenseArgs> LicenseObservable
        {
            get
            {
                return Observable
                    .FromEventPattern<LicenseArgs>(
                        h => this.OnLicenseChanged += h,
                        h => this.OnLicenseChanged -= h)
                    .Select(x => x.EventArgs);
            }
        }

        public event EventHandler<KeysArgs> OnKeysChanged;

        public IObservable<KeysArgs> KeysObservable
        {
            get
            {
                return Observable
                    .FromEventPattern<KeysArgs>(
                        h => this.OnKeysChanged += h,
                        h => this.OnKeysChanged -= h)
                    .Select(x => x.EventArgs);
            }
        }

        public void CreateLicense(string customer, string email)
        {

            var license = License.New()
                            .WithUniqueIdentifier(Guid.NewGuid())
                            .As(LicenseType.Standard)
                            .ExpiresAt(DateTime.Now.AddDays(30))
                            .WithMaximumUtilization(1)
                            .LicensedTo(customer, email)
                            .CreateAndSignWithPrivateKey(m_privateKey, m_password);

            string licenseFilename = GetSaveLicenseFileName();

            if (!string.IsNullOrEmpty(licenseFilename))
            {
                File.WriteAllText(licenseFilename, license.ToString(), Encoding.UTF8);
            }
        }

        private string GetSaveLicenseFileName()
        {
            // Configure save file dialog box
            var dlg = new SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                FileName = "License",
                DefaultExt = ".lic",
                Filter = "License File (.lic)|*.lic"
            };
            bool? result = dlg.ShowDialog();

            if (result == null || !result.Value)
                return string.Empty;
            return dlg.FileName;
        }

        public void LoadLicense()
        {
            string licenseFilename = GetLoadLicenseFileName();
            License license;
            using (var streamReader = new StreamReader(licenseFilename))
            {
                license = License.Load(streamReader);
            }
            var validationFailures = license.Validate()
                                    .ExpirationDate()
                                    .And()
                                    .Signature(m_publicKey)
                                    .AssertValidLicense()
                                    .ToList();

            
            var sb = new StringBuilder();
            if (validationFailures.Any())
            {
                foreach (var failure in validationFailures)
                    sb.AppendLine(failure.GetType().Name + ": " + failure.Message + " - " + failure.HowToResolve);

            }
            OnLicenseChanged?
                .Invoke(this, new LicenseArgs(license.Customer.Name, license.Customer.Email, sb.ToString()));
        }

        public bool HasKeys()
        {
            return m_privateKey != null;
        }

        private string GetLoadLicenseFileName()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Multiselect = false,
                Filter = "License File (.lic)|*.lic|All files (*.*)|*.*",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };
            bool? result = openFileDialog.ShowDialog();

            if (result == null || !result.Value)
                return string.Empty;
            return openFileDialog.FileName;
        }
    }
}