﻿using System;
using System.Reactive.Linq;
using Model;
using Reactive.Bindings;

namespace ViewModel
{
    public interface IMainViewModel
    {
        void Start();
    }

    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private readonly IMainModel m_model;

        public MainViewModel(IMainModel model)
        {
            m_model = model;
            Password.Value = "Enghouse Interactive Password";
            CreateKeysCommand
                .Subscribe(_ => { m_model.CreateKeys(Password.Value); });

            PublicKey = model
                .KeysObservable
                .Select(x => x.PublicKey)
                .ToReadOnlyReactiveProperty();
            PrivateKey = model
                .KeysObservable
                .Select(x => x.PrivateKey)
                .ToReadOnlyReactiveProperty();

            // License Server
            Customer.Value = "Charles Babbage";
            Email.Value = "sip:charles.babbage@enghouse.com";

            CreateLicenseCommand
                .Subscribe(_ => { m_model.CreateLicense(Customer.Value, Email.Value); });

            // Application

            ApplicationCustomer = model.LicenseObservable
                .Select(x => x.Customer)
                .ToReadOnlyReactiveProperty();
            ApplicationEmail = model.LicenseObservable
                .Select(x => x.Email)
                .ToReadOnlyReactiveProperty();
            ApplicationErrors = model.LicenseObservable
                .Select(x => x.Errors)
                .ToReadOnlyReactiveProperty();
            
            LoadLicenseCommand
                .Subscribe(_ => { m_model.LoadLicense(); });
        }


        // Key Management

        public ReactiveProperty<string> Password { get; set; } = new ReactiveProperty<string>();
        public ReactiveCommand CreateKeysCommand { get; } = new ReactiveCommand();
        public ReadOnlyReactiveProperty<string> PrivateKey { get; set; }
        public ReadOnlyReactiveProperty<string> PublicKey { get; set; }


        // License Creation
        public ReactiveCommand CreateLicenseCommand { get; } = new ReactiveCommand();

        public ReactiveProperty<string> Customer { get; } = new ReactiveProperty<string>();
        public ReactiveProperty<string> Email { get; } = new ReactiveProperty<string>();

        // Application

        public ReactiveCommand LoadLicenseCommand { get; } = new ReactiveCommand();

        public ReadOnlyReactiveProperty<string> ApplicationCustomer { get; }
        public ReadOnlyReactiveProperty<string> ApplicationEmail { get; }
        public ReadOnlyReactiveProperty<string> ApplicationErrors { get; }


        public void Start()
        {
            m_model.Start();
        }
    }
}