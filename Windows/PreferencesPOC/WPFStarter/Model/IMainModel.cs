﻿using System;

namespace Model
{
    public interface IMainModel
    {
        void Start();
        string Name { get; set; }
        event EventHandler<GreetingArgs> OnGreetingChanged;
        void SaveSettings();
    }
}