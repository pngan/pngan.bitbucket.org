﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Model
{
    public class MainModel : IMainModel
    {
        public void Start()
        {
            var json = File.ReadAllText(@"settings.json");
            JsonConvert.PopulateObject(json, this);
        }

        private string m_name;

        public string Name
        {
            get { return m_name; }
            set
            {
                m_name = value;
                OnGreetingChanged?.Invoke(this, new GreetingArgs(m_name));
            }
        }
        
        public event EventHandler<GreetingArgs> OnGreetingChanged;
        public void SaveSettings()
        {
            string json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(@"settings.json", json);
        }
    }
}