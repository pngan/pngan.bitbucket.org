﻿using System;
using System.Windows.Input;
using Model;

namespace ViewModel
{
    public interface IMainViewModel
    {
        void Start();
        void SaveSettings();
    }


    public class SaveCommand : ICommand {
        private readonly Action m_saveAction;

        public SaveCommand(Action saveAction)
        {
            m_saveAction = saveAction;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            m_saveAction?.Invoke();
        }

        public event EventHandler CanExecuteChanged;
    }


    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private readonly IMainModel m_model;

        public MainViewModel(IMainModel model)
        {
            m_model = model;
            m_model.OnGreetingChanged += OnGreetingChanged;
            SaveCommand = new SaveCommand(SaveSettings);
        }

        private void OnGreetingChanged(object sender, GreetingArgs e)
        {
            Greeting = e.Greeting;
        }

        private string m_name;
        private string m_greeting;

        public string Name
        {
            get { return m_name; }
            set
            {
                m_name = value;
                m_model.Name = value;
            }
        }

        public string Greeting
        {
            get { return m_greeting; }
            set
            {
                m_greeting = value;
                OnPropertyChanged("Greeting");
            }
        }
        

        public ICommand SaveCommand { get; }


        private void SaveSettings()
        {
            m_model.SaveSettings();
        }

        public void Start()
        {
            m_model.Start();
            Greeting = m_model.Name;
        }

        void IMainViewModel.SaveSettings()
        {
            throw new NotImplementedException();
        }
    }
}