﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Management;
using System.Net.Sockets;
using System.IO;

namespace vlcmote
{
    enum VlcCommand
    {
        Quit
    }

    public class VlcRemote
    {
        // maximum 2 second wait on results. 
        const int WaitTimeout = 2000;

        static ASCIIEncoding ASCIIEncoding = new ASCIIEncoding();

        Process vlcProcess;
        TcpClient client;

        static int GetParentProcessId(int Id)
        {
            int parentPid = 0;
            using (ManagementObject mo = new ManagementObject("win32_process.handle='"
            + Id.ToString() + "'"))
            {
                mo.Get();
                parentPid = Convert.ToInt32(mo["ParentProcessId"]);
            }
            return parentPid;
        }


        public VlcRemote(string filename)
        {

            string vlcPath = null;
            var vlcKey = Registry.LocalMachine.OpenSubKey(@"Software\VideoLan\VLC");
            if (vlcKey == null)
            {
                vlcKey = Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\VideoLan\VLC");
            }
            if (vlcKey != null)
            {
                vlcPath = vlcKey.GetValue(null) as string;
            }

            if (vlcPath == null)
            {
                throw new ApplicationException("Can not find the VLC executable!");
            }

            var info = new ProcessStartInfo(vlcPath, string.Format(@" screen:// --screen-fps=12  -I dummy --extraintf rc --rc-host localhost:8088 --rc-quiet --no-sout-audio --sout #transcode{{venc=x264,quality:10,scale=1,fps=12}}:duplicate{{dst=std{{access=file,mux=mp4,dst=\temp\{0}.avi}}}}", filename));
            //var info = new ProcessStartInfo(vlcPath, string.Format(@"screen:// --screen-fps=12  -I dummy --extraintf rc --rc-host localhost:8088 --rc-quiet --no-sout-audio --sout #transcode{{venc=theora,quality:100,scale=1,fps=12}}:duplicate{{dst=std{{access=file,mux=ogg,dst=\temp\{0}.ogg}}}}}}", filename));
            //var info = new ProcessStartInfo(vlcPath, string.Format(@" screen:// -I dummy  --extraintf rc --rc-host localhost:8088 --rc-quiet --screen-fps=12 :sout=#transcode{{vcodec=WMV2,vb=1800,scale=1}}:std{{access=file,mux=asf,dst=\temp\{0}.wmv}}", filename));


            vlcProcess = Process.Start(info);
            client = new TcpClient("localhost", 8088);
        }

        public Process VlcPlaybackProcess
        {
            get
            {
                var currentProcessId = Process.GetCurrentProcess().Id;
                Process vlcProcess = null;
                foreach (var process in Process.GetProcessesByName("vlc"))
                {
                    int parentId;
                    try
                    {
                        parentId = GetParentProcessId(process.Id);
                    }
                    catch
                    {
                        return null;
                    }
                    if (parentId == currentProcessId)
                    {
                        vlcProcess = process;
                        break;
                    }
                }
                return vlcProcess;

            }
        }

        public void Quit()
        {
            SendCommand(VlcCommand.Quit);
        }

        string WaitForResult()
        {
            string result = "";
            DateTime start = DateTime.Now;
            while ((DateTime.Now - start).TotalMilliseconds < WaitTimeout)
            {
                result = ReadTillEnd();
                if (!string.IsNullOrEmpty(result))
                {
                    break;
                }
            }
            return result;
        }

        void SendCommand(VlcCommand command)
        {
            SendCommand(command, null);
        }

        void SendCommand(VlcCommand command, string param)
        {
            // flush old stuff
            ReadTillEnd();

            string packet = Enum.GetName(typeof(VlcCommand), command).ToLower();
            if (param != null)
            {
                packet += " " + param;
            }
            packet += Environment.NewLine;

            var buffer = ASCIIEncoding.GetBytes(packet);
            client.GetStream().Write(buffer, 0, buffer.Length);
            client.GetStream().Flush();


            Trace.Write(packet);

        }

        public string ReadTillEnd()
        {
            StringBuilder sb = new StringBuilder();
            while (client.GetStream().DataAvailable)
            {
                int b = client.GetStream().ReadByte();
                if (b >= 0)
                {
                    sb.Append((char)b);
                }
                else
                {
                    break;
                }
            }
            return sb.ToString();
        }

    }
}
