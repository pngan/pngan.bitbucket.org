﻿using System;
using System.IO;
using NUnit.Framework;
using System.Threading.Tasks;

namespace ScreenCapture
{
    [TestFixture]
    public class UnitTest1
    {
        vlcmote.VlcRemote m_vlcRemote;

        [SetUp]
        public void SetupMethod()
        {
            m_vlcRemote = new vlcmote.VlcRemote(TestContext.CurrentContext.Test.Name);
        }

        [TearDown]
        public async void TeardownMethod()
        {
            if (m_vlcRemote == null)
                return;

            m_vlcRemote.Quit();
            if (TestContext.CurrentContext.Result.Status == TestStatus.Failed)
            {
                while ( m_vlcRemote.VlcPlaybackProcess != null)
                {
                    await Task.Delay(500);
                }
                var filename =  string.Format( "\\temp\\{0}.avi", TestContext.CurrentContext.Test.Name );
                if ( File.Exists( filename))
                {
                    File.Delete(filename);
                }
            }
        }

        [Test]
        public async void TestMethod1()
        {
            await Task.Delay(64000);
            //Assert.IsTrue(false);
        }
    }
}
