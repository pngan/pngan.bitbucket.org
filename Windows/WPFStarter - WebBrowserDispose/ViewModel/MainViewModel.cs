﻿using System;
using System.Diagnostics;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Model;
using Reactive.Bindings;

namespace ViewModel
{
    public interface IMainViewModel
    {
        void Start();
    }

    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private readonly IMainModel m_model;

        public MainViewModel(IMainModel model)
        {
            m_model = model;
            Greeting = model.GreetingObservable.Select(x => x.Greeting).ToReadOnlyReactiveProperty();
            Name.Subscribe(m_model.SetName);
        }

        public ReactiveProperty<string> Name { get; set; } = new ReactiveProperty<string>();

        public ReadOnlyReactiveProperty<string> Greeting { get; }

        
        public void Start()
        {
            m_model.Start();
        }
    }
}