﻿using System;
using System.Diagnostics;
using System.Windows;
using Microsoft.Win32;
using ViewModel;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IMainViewModel m_viewModel;

        public MainWindow()
        {
            SetBrowserEmulationMode();
            InitializeComponent();
        }

        public MainWindow(IMainViewModel viewModel) : this()
        {
            m_viewModel = viewModel;
            viewModel.Start();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            ((FrameworkElement)sender).DataContext = m_viewModel;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var win = new WebWindow();
            win.Show();
            win.Navigate("http://www.bayparcplaza.com");
        }
        public void SetBrowserEmulationMode()
        {
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            if (String.Compare(fileName, "devenv.exe", true) == 0 || String.Compare(fileName, "XDesProc.exe", true) == 0)
                return;
            UInt32 mode = 10000;
            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, mode);
        }

        private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(appName, (UInt32)value, RegistryValueKind.DWord);
            }
        }
    }
}
