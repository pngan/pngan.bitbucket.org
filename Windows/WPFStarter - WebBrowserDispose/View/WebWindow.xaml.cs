﻿using System;
using System.Windows;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class WebWindow : Window
    {
        public WebWindow()
        {
            InitializeComponent();
        }

        public void Navigate(string url)
        {
            _browser.Navigate(url);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            //_browser.Dispose();
            base.OnClosed(e);
        }
    }
}
