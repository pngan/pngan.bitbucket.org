﻿using System;
using System.Globalization;
using LogExpert;

namespace Columnizer
{
    /// <summary>
    ///     This is an example implementation of the ILogLineColumnizer interface.
    ///     The TimeStampColumnizer splits the log line into 3 fields (date, time, message).
    ///     It can parse timestamps on the format dd.MM.yy HH:mm:ss,fff.
    /// </summary>
    /// <remarks>
    ///     The callback functions are not used in this example.
    /// </remarks>
    public class TimeStampColumnizer : ILogLineColumnizer
    {
        protected const string DatetimeFormat = "HH:mm:ss.ff";
        protected CultureInfo CultureInfo = new CultureInfo("en-US");
        protected int TimeOffset;

        /// <summary>
        ///     Implement this property to let LogExpert display the name of the Columnizer
        ///     in its Colummnizer selection dialog.
        /// </summary>
        public string Text
        {
            get { return GetName(); }
        }

        #region ILogLineColumnizer Member

        public int GetColumnCount()
        {
            return 2;
        }

        public string[] GetColumnNames()
        {
            return new[] {"Time", "Log-Message"};
        }

        public string GetDescription()
        {
            return "This columnizer that splits a line into 2 columns." +
                   "Timestamp parsing is supported for the format 'HH:mm:ss.ff'.";
        }

        public string GetName()
        {
            return "Enghouse Columnizer";
        }

        public int GetTimeOffset()
        {
            return TimeOffset;
        }

        /// <summary>
        ///     This function has to return the timestamp of the given log line.
        ///     It takes a substring of the line (first 11 chars containing the time) and converts this into a
        ///     DateTime object.
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        public DateTime GetTimestamp(ILogLineColumnizerCallback callback, string line)
        {
            // A first check if this could be a valid time string. This is for performance reasons, because 
            // DateTime.ParseExact() is slow when receiving an invalid input.
            if (IsValidTimestamp(line))
            {
                return DateTime.MinValue;
            }

            try
            {
                // Parse into a DateTime
                var dateTime = DateTime.ParseExact(line.Substring(0, 11), DatetimeFormat, CultureInfo);

                // Add the time offset before returning
                return dateTime.AddMilliseconds(TimeOffset);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }

        public bool IsTimeshiftImplemented()
        {
            return true;
        }


        /// <summary>
        ///     This function is called if the user changes a value in a column (edit mode in the log view).
        ///     The purpose if the function is to determine a new timestamp offset. So you have to handle the
        ///     call only if the given column displays a timestamp.
        /// </summary>
        public void PushValue(ILogLineColumnizerCallback callback, int column, string value, string oldValue)
        {
            if (column == 0)
            {
                try
                {
                    var newDateTime = DateTime.ParseExact(value, DatetimeFormat, CultureInfo);
                    var oldDateTime = DateTime.ParseExact(oldValue, DatetimeFormat, CultureInfo);
                    var mSecsOld = oldDateTime.Ticks/TimeSpan.TicksPerMillisecond;
                    var mSecsNew = newDateTime.Ticks/TimeSpan.TicksPerMillisecond;
                    TimeOffset = (int) (mSecsNew - mSecsOld);
                }
                catch (FormatException)
                {
                }
            }
        }

        public void SetTimeOffset(int msecOffset)
        {
            TimeOffset = msecOffset;
        }


        /// <summary>
        ///     Given a single line of the logfile this function splits the line content into columns. The function returns
        ///     a string array containing the splitted content.
        /// </summary>
        /// <remarks>
        ///     This function is called by LogExpert for every line that has to be drawn in the grid view. The faster your code
        ///     handles the splitting, the faster LogExpert can draw the grid view content.
        /// </remarks>
        /// <param name="callback">Callback interface with functions which can be used by the columnizer</param>
        /// <param name="line">The line content to be splitted</param>
        public string[] SplitLine(ILogLineColumnizerCallback callback, string line)
        {
            var cols = new string[2] {"", ""};

            // A first check if this could be a valid date/time string. This is for performance reasons, because 
            // DateTime.ParseExact() is slow when receiving an invalid input.
            // If there's no valid time, the content will be returned in column 1. Date and time column will be left blank.
            if (IsValidTimestamp(line))
            {
                cols[1] = line;
                return cols;
            }

            // If the time offset is not 0 we have to do some more work:
            // - parse the date/time part of the log line
            // - add the time offset
            // - convert back to string
            if (TimeOffset != 0)
            {
                try
                {
                    var dateTime = DateTime.ParseExact(line.Substring(0, 11), DatetimeFormat, CultureInfo);
                    dateTime = dateTime.Add(new TimeSpan(0, 0, 0, 0, TimeOffset));
                    var newDate = dateTime.ToString(DatetimeFormat);
                    cols[0] = newDate.Substring(0, 11); // time
                }
                catch (Exception)
                {
                    cols[0] = "n/a";
                }
            }
            else
            {
                cols[0] = line.Substring(0, 11); // time
            }
            cols[1] = line.Substring(12); // rest of line
            return cols;
        }

        private bool IsValidTimestamp(string line)
        {
            // 0         1         2         3         4         5         6         7         8         9         10        11 
            // 0123456789012345678901234567890
            // 15:07:15.28 this is a log message line
            if (line.Length < 11)
                return false;

            return line[2] == ':' && line[5] == ':' && line[8] != '.';
        }


        #endregion
    }
}