﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;

namespace View.Interactivity
{

    public class SizeChangedAnimationBehavior : Behavior<FrameworkElement>
    {
        public Storyboard HeightIncreasedStoryboard
        {
            get { return (Storyboard)GetValue(HeightIncreasedStoryboardProperty); }
            set { SetValue(HeightIncreasedStoryboardProperty, value); }
        }

        public static readonly DependencyProperty HeightIncreasedStoryboardProperty
            = DependencyProperty.Register("HeightIncreasedStoryboard", typeof(Storyboard),
            typeof(SizeChangedAnimationBehavior));
        
        public Storyboard HeightDecreasedStoryboard
        {
            get { return (Storyboard)GetValue(HeightDecreasedStoryboardProperty); }
            set { SetValue(HeightDecreasedStoryboardProperty, value); }
        }

        public static readonly DependencyProperty HeightDecreasedStoryboardProperty
            = DependencyProperty.Register("HeightDecreasedStoryboard", typeof(Storyboard),
            typeof(SizeChangedAnimationBehavior));
        
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SizeChanged += AssociatedObject_SizeChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.SizeChanged -= AssociatedObject_SizeChanged;

        }

        private void AssociatedObject_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            
            if (e.HeightChanged)
            {
                if (e.NewSize.Height > e.PreviousSize.Height)
                    HeightIncreasedStoryboard?.Begin();
                else
                    HeightDecreasedStoryboard?.Begin();
            }
        }
    }

}
