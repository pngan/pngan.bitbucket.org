﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace LogExpert
{
    public class DateTimeColumnizer : IColumnizerConfigurator, ILogLineColumnizer, IInitColumnizer
    {
        private DateTimeColumnizerConfig _config = new DateTimeColumnizerConfig();
        private string _name = "";
        protected string ConfigDir;
        protected int TimeOffset;
        ///// <summary>
        ///// Implement this property to let LogExpert display the name of the Columnizer
        ///// in its Colummnizer selection dialog.
        ///// </summary>
        public string Text
        {
            get { return GetName(); }
        }

        #region IColumnizerConfigurator Members

        public void Configure(ILogLineColumnizerCallback callback, string configDirectory)
        {
            var configPath = configDirectory + @"\DateTimeColumnizer-" + _name + "." + ".dat";
            var dlg = new DateTimeColumnizerConfigDlg(_config);

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                dlg.Apply(_config);
                var formatter = new BinaryFormatter();
                Stream fs = new FileStream(configPath, FileMode.Create, FileAccess.Write);
                formatter.Serialize(fs, _config);
                fs.Close();
            }
        }

        public void LoadConfig(string configDirectory)
        {
            ConfigDir = configDirectory;
        }

        #endregion

        #region ILogLineColumnizer Members

        public int GetColumnCount()
        {
            return 2;
        }

        public string[] GetColumnNames()
        {
            return new[] {"Time", "Message"};
        }

        public string GetDescription()
        {
            return "DateTime Columnizer";
        }

        public string GetName()
        {
            return "DateTimeColumnizer";
        }

        public int GetTimeOffset()
        {
            return TimeOffset;
        }

        public DateTime GetTimestamp(ILogLineColumnizerCallback callback, string line)
        {
            DateTime timestamp;
            SplitLineWithTimestamp(callback, line, out timestamp);
            return timestamp.AddMilliseconds(TimeOffset);

            //return timestamp;
        }

        public bool IsTimeshiftImplemented()
        {
            return true;
        }

        public void PushValue(ILogLineColumnizerCallback callback, int column, string value, string oldValue)
        {
            if (column == 0) // First column is the timestamp
            {
                DateTime newDateTime;
                SplitLineWithTimestamp(value, out newDateTime);
                if (newDateTime == DateTime.MinValue)
                    return;

                DateTime oldDateTime;
                SplitLineWithTimestamp(oldValue, out oldDateTime);
                if (oldDateTime == DateTime.MinValue)
                    return;

                TimeOffset = (int) (newDateTime - oldDateTime).TotalMilliseconds;
            }
        }

        public void SetTimeOffset(int msecOffset)
        {
            TimeOffset = msecOffset;
        }


        private string[] PopulateArrayWithNoDatetime(string message)
        {
            return PopulateParsedArray(string.Empty, message);
        }

        private string[] PopulateParsedArray(string timestamp, string message)
        {
            return new[] {timestamp, message};
        }

        public string[] SplitLine(ILogLineColumnizerCallback callback, string line)
        {
            DateTime timestamp;
            return SplitLineWithTimestamp(callback, line, out timestamp);
        }

        private string[] SplitLineWithTimestamp(ILogLineColumnizerCallback callback, string line, out DateTime timestamp)
        {
            return SplitLineWithTimestamp(line, out timestamp);
        }

        private string[] SplitLineWithTimestamp(string line, out DateTime timestamp)
        {
            // Try all the time format string in order until one matches the timestamp in the input line
            foreach (var dateTimeFormatString in _config.DateTimeFormatStrings)
            {
                if (string.IsNullOrEmpty(dateTimeFormatString))
                    continue;

                if (line.Length < dateTimeFormatString.Length)
                    continue;

                var dateTimeString = line.Substring(0, dateTimeFormatString.Length);

                if (DateTime.TryParseExact(
                    dateTimeString, dateTimeFormatString,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out timestamp))
                {
                   // timestamp = timestamp.AddMilliseconds(TimeOffset);
                    return PopulateParsedArray(dateTimeString, line.Substring(dateTimeFormatString.Length));
                }
            }
            timestamp = DateTime.MinValue;
            return PopulateArrayWithNoDatetime(line);
        }

        #endregion

        #region IInitColumnizer Members

        public void DeSelected(ILogLineColumnizerCallback callback)
        {
            //throw new NotImplementedException();
        }

        public void Selected(ILogLineColumnizerCallback callback)
        {
            var fi = new FileInfo(callback.GetFileName());

            _name = BitConverter.ToString(new MD5CryptoServiceProvider().
                ComputeHash(Encoding.Unicode.GetBytes(fi.FullName))).Replace("-", "").ToLower();


            var configPath = ConfigDir + @"\DateTimeColumnizer-" + _name + "." + ".dat";

            if (!File.Exists(configPath))
            {
                _config = new DateTimeColumnizerConfig();
            }
            else
            {
                Stream fs = File.OpenRead(configPath);
                var formatter = new BinaryFormatter();
                try
                {
                    var config = (DateTimeColumnizerConfig) formatter.Deserialize(fs);
                    if (config.IsValid())
                    {
                        _config = config;
                    }
                }
                catch (SerializationException e)
                {
                    MessageBox.Show(e.Message, "Deserialization Error");
                    _config = new DateTimeColumnizerConfig();
                }
                finally
                {
                    fs.Close();
                }
            }
        }

        #endregion
    }
}