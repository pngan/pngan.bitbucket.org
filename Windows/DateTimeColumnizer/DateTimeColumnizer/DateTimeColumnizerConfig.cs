﻿using System;

namespace LogExpert
{
    [Serializable]
    public class DateTimeColumnizerConfig
    {
        public string[] DateTimeFormatStrings
        {
            get; 
            set;
        }

        public DateTimeColumnizerConfig()
        {
            DateTimeFormatStrings = new[] {"HH:mm:ss.ff", "yyyy-MM-dd HH:mm:ss"};
        }

        internal bool IsValid()
        {
            if (DateTimeFormatStrings == null)
                return false;

            foreach (var dateTimeFormatString in DateTimeFormatStrings)
            {
                if (string.IsNullOrEmpty(dateTimeFormatString))
                    return false;
            }

            return true;
        }
    }
}
