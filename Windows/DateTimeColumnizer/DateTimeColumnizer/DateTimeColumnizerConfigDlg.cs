﻿using System;
using System.Windows.Forms;

namespace LogExpert
{
    public partial class DateTimeColumnizerConfigDlg : Form
    {
        private readonly DateTimeColumnizerConfig _config;
        private const string NewLineDelimiter = "\r\n";

        public DateTimeColumnizerConfigDlg(DateTimeColumnizerConfig config)
        {
            _config = config;
            InitializeComponent();
            formatStringsTextBox.Text = string.Join(NewLineDelimiter, _config.DateTimeFormatStrings);
        }

        internal void Apply(DateTimeColumnizerConfig config)
        {
            config.DateTimeFormatStrings = formatStringsTextBox.Text.Split(NewLineDelimiter.ToCharArray(),
                StringSplitOptions.RemoveEmptyEntries);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Apply(_config);
        }

        private void DateTimeColumnizerConfigDlg_Load(object sender, EventArgs e)
        {
            ValidateChildren();
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            Apply(_config);
        }
    }
}