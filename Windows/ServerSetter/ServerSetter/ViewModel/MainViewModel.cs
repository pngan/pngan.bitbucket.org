using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Data;
using System.Windows.Input;
using System.Security.Principal;

namespace ServerSetter.ViewModel
{

    public class ContextMenuVM
    {
        public string Displayname { get; set; }
        public RelayCommand MyContextMenuCommand { get; set; }
    }

    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public class ServerItem
        {
            public bool IsChecked { get; set; }
            public string ServerName { get; set; }
        }


        public MainViewModel()
        {
            CheckRunningAsAdministrator();

            var names = new List<string>(ConfigurationManager.AppSettings["Servers"].Split(new char[] { ';' }));
            var selected = ReadServerRegistryValue();
            ServerItem selectedItem = null;

            foreach (var name in names)
            {
                bool isChecked = string.Equals(selected, name);
                var item = new ServerItem { ServerName = name, IsChecked = isChecked };
                if ( isChecked)
                    selectedItem = item;
                Items.Add(item);
            }


            ItemsView = CollectionViewSource.GetDefaultView(Items);
            ItemsView.MoveCurrentTo(selectedItem);
            ItemsView.CurrentChanged += OnItemsChanged;


            Commands.Add(new ContextMenuVM { Displayname = "Configure", MyContextMenuCommand = new RelayCommand(ConfigureAction, () => true) });
            Commands.Add(new ContextMenuVM { Displayname = "Exit", MyContextMenuCommand = new RelayCommand(ExitAction, () => true) });

            _showHideWindowCommand = new RelayCommand( ExecuteShowHideWindow);
            ConfigureCommand = new RelayCommand(ConfigureAction);
            _isVisible = true;
        }

        private void ConfigureAction()
        {
            EditConfigurationFile();
            PromptUserForApplicationRestart();
        }

        private static void PromptUserForApplicationRestart()
        {
            System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show("The application must be restarted for the changes to take effect.",
                "Restart ServerSetter", System.Windows.MessageBoxButton.OKCancel, System.Windows.MessageBoxImage.Question);
            if (result == System.Windows.MessageBoxResult.OK)
            {
                System.Diagnostics.Process.Start(System.Windows.Application.ResourceAssembly.Location);
                System.Windows.Application.Current.Shutdown();
            }
        }

        private static void CheckRunningAsAdministrator()
        {
            if (!IsAdministrator())
            {
                System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show("The application must be running as Administrator.",
                    "Run As Administrator", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Exclamation);
                if (result == System.Windows.MessageBoxResult.OK)
                {
                    System.Windows.Application.Current.Shutdown();
                }
            }
        }

        private static void EditConfigurationFile()
        {
            System.Diagnostics.ProcessStartInfo procStIfo =
                 new System.Diagnostics.ProcessStartInfo("notepad", "ServerSetter.exe.config");

            procStIfo.RedirectStandardOutput = true;
            procStIfo.UseShellExecute = false;

            procStIfo.CreateNoWindow = true;

            System.Diagnostics.Process proc = new System.Diagnostics.Process();

            proc.StartInfo = procStIfo;

            proc.Start();
            proc.WaitForExit();
        }

        public void ExitAction()
        {
            System.Windows.Application.Current.Shutdown();
        }

        public ObservableCollection<ContextMenuVM> CommandList { get; set; }

        private void ExecuteShowHideWindow()
        {
            IsVisible = !IsVisible;
        }

        public RelayCommand ShowHideWindowCommand { get { return _showHideWindowCommand; } } 

        private void WriteServerRegistryValue(string serverName)
        {
            var baseRegKey = ConfigurationManager.AppSettings["BaseRegKey"];
            var regKey = ConfigurationManager.AppSettings["RegKey"];

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
            using (var baseKey = hklm.OpenSubKey(baseRegKey, true))
            {
                if (baseKey == null)
                {
                    using (var newKey = hklm.CreateSubKey(baseRegKey, RegistryKeyPermissionCheck.ReadWriteSubTree))
                        newKey.SetValue(regKey, serverName);
                }
                else
                {
                    baseKey.SetValue(regKey, serverName);
                }
            }
        }

        private string ReadServerRegistryValue()
        {
            var baseRegKey = ConfigurationManager.AppSettings["BaseRegKey"];
            var regKey = ConfigurationManager.AppSettings["RegKey"];
            string server = null;

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
            using (var key = hklm.OpenSubKey(baseRegKey))
            {
                if (key != null)
                    server = (string)key.GetValue(regKey, null);
            }
            return server;
        }

        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private ObservableCollection<ServerItem> _items = new ObservableCollection<ServerItem>();
        private ObservableCollection<ContextMenuVM> _commands = new ObservableCollection<ContextMenuVM>();
        private ServerItem _currentItem;
        private bool _isVisible;
        private RelayCommand _showHideWindowCommand;
        public RelayCommand ConfigureCommand { get; set; }


        public ObservableCollection<ServerItem> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                RaisePropertyChanged("Items");
            }
        }
        public ObservableCollection<ContextMenuVM> Commands
        {
            get { return _commands; }
            set
            {
                _commands = value;
                RaisePropertyChanged("Commands");
            }
        }

        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                RaisePropertyChanged("IsVisible");
            }
        }

        public ICollectionView ItemsView { get; private set; }


        private void OnItemsChanged(object sender, EventArgs e)
        {
            var selectedItem = ItemsView.CurrentItem as ServerItem;
            if (selectedItem == null) return;

            // Uncheck all items
            foreach( var item in _items )
            {
                item.IsChecked = false;
            }

            // Check the selected item and write to registry
            selectedItem.IsChecked = true;
            WriteServerRegistryValue(selectedItem.ServerName);
        }
    }

}