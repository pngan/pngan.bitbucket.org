namespace ConsolePOC.Model
{
    public enum ConversationState
    {
        Idle, Offering, Connected
    }
}