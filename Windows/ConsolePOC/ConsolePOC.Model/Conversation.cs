﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePOC.Model
{
    public class Conversation : IConversation
    {

        private string m_displayName;
        private ConversationState m_state;
        private string v;

        public Conversation(string v)
        {
            this.v = v;
        }

        public string DisplayName
        {
            get { return m_displayName; }
        }

        public ConversationState State
        {
            get { return m_state; }
            set { m_state = value; }
        }
    }
}
