namespace ConsolePOC.Model
{
    public interface IConversation
    {
        string DisplayName { get; }
        ConversationState State { get; set; }
    }
}