namespace ConsolePOC.Model
{
    public interface IConversationAction
    {
        Conversation Conversation { get; }
        ConversationState NewState { get; }
    }
}