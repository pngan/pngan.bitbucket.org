namespace ConsolePOC.Model
{
    public interface IApplication
    {
        IConversationAction CreateConversation();
        IConversationAction OfferConversation(IConversation conversation);
        IConversationAction ConnectConversation(IConversation conversation);
        IConversationAction CompleteConversation(IConversation conversation);
    }
}