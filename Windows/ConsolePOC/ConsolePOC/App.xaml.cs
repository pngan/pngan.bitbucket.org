﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Resources;
using System.Windows;
using Autofac;
using System.Reflection;
using ConsolePOC.Logging;
using Serilog;

namespace ConsolePOC
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IContainer _container;

        private void OnAppStartup(object sender, StartupEventArgs e)
        {
            

        }


        private void BuildIoCContainerFromPlugins()
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            var builder = new ContainerBuilder();
            builder.Register(c => SerilogConfig.CreateLogger())
               .As<ILogger>()
               .SingleInstance();
            builder.RegisterModule<SerilogLoggingModule>();
            builder.RegisterAssemblyTypes(thisAssembly).AsImplementedInterfaces();

            builder.RegisterType<MainWindow>();
            builder.RegisterType<MainViewModel>();

            _container = builder.Build();
        }
    }
}
