﻿using System;
using ConsolePOC.Model;
using ConsolePOC.ViewModel;

namespace ConsolePOC.Wrapper
{
    public class ConversationWrapper : Observable
    {
        private IConversation m_model;

        public ConversationWrapper(IConversation model)
        {
            if (model==null)
                throw new ArgumentNullException("model should not be null");
            Model = model;
        }

        public ConversationState State
        {
            get { return Model.State; }
            set
            {
                if (!Equals(Model.State, value))
                {
                    Model.State = value;
                    OnPropertyChanged();
                }
            }
        }
        
        public IConversation Model
        {
            get { return m_model; }
            private set
            {
                m_model = value;
            }
        }
    }
}
