﻿namespace ConsolePOC
{
    public interface IMainViewModel { }
    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        public DelegateCommand FullScreenCommand { get; private set; }
        public DelegateCommand NormalScreenCommand { get; private set; }

        private bool _isFullScreen = false;
        public bool IsFullScreen
        {
            get { return _isFullScreen; }
            set
            {
                _isFullScreen = value;
                OnPropertyChanged("IsFullScreen");
            }
        }


        private object _displayViewModel;

        public object DisplayViewModel
        {
            get { return _displayViewModel; }
            set { SetField(ref _displayViewModel, value); }
        }

        public MainViewModel( )
        {

        }

        private void OnNormalScreenCommand()
        {
            IsFullScreen = false;
        }

        private void OnFullScreenCommand()
        {
            IsFullScreen = true;
        }
    }
}