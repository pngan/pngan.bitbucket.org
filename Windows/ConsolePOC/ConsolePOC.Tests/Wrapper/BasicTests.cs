﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsolePOC.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsolePOC.Model;

namespace ConsolePOC.Wrapper.Tests
{
    [TestClass()]
    public class ConversationWrapperTests
    {
        private Conversation m_conversation;

        [TestInitialize]
        public void Initialize()
        {
            m_conversation = new Conversation("joe Bloggs");

        }
        [TestMethod()]
        public void ShouldContainModelInModelProperty()
        {
            var wrapper = new ConversationWrapper(m_conversation);
            Assert.AreEqual(m_conversation, wrapper.Model);
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ShouldThrowArgumentNullExceptionIfModelIsNull()
        {
            var wrapper = new ConversationWrapper(null);
        }

        [TestMethod]
        public void ShouldSetValueOfUnderlyingModelProperty()
        {
            var wrapper = new ConversationWrapper(m_conversation) {State = ConversationState.Connected};
            Assert.AreEqual(ConversationState.Connected, m_conversation.State);


        }

    }
}