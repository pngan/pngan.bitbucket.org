﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsolePOC.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit;

namespace ConsolePOC.Wrapper
{
    [TestClass]
    public class ChangeNotificationSimpleProperty
    {
        private Conversation m_conversation;

        [TestInitialize]
        public void Initialize()
        {
            m_conversation = new Conversation("joe Bloggs");

        }

        [TestMethod]
        public void ShouldRaisePropertyChangedEventOnPropertyChanged()
        {
            var fired = false;
            var wrapper = new ConversationWrapper(m_conversation);
            wrapper.PropertyChanged += (o, args) =>
            {
                if (args.PropertyName == "State")
                {
                    fired = true;
                }
            };

            wrapper.State = ConversationState.Connected;

            Assert.IsTrue(fired);
        }
        [TestMethod]
        public void ShouldNotRaisePropertyChangedEvenIfPropertyIsSetToSameValue()
        {
            var fired = false;
            var wrapper = new ConversationWrapper(m_conversation);
            wrapper.PropertyChanged += (o, args) =>
            {
                if (args.PropertyName == "State")
                {
                    fired = true;
                }
            };

            wrapper.State = ConversationState.Idle;

            Assert.IsFalse(fired);
        }
    }
   
}
