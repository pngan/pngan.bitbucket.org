﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.ComponentModel;
using Microsoft.Win32;

namespace CtiServerSelector
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MyNotifyIcon_PreviewTrayContextMenuOpen(object sender, RoutedEventArgs e)
        {
            if (_menu.Items == null || _menu.Items.Count == 0)
            {
                var names = new List<string>(ConfigurationManager.AppSettings["Servers"].Split(new char[] { ';' }));
                foreach (var name in names)
                {
                    MenuItem newMenuItem1 = new MenuItem();
                    newMenuItem1.Header = name;
                    newMenuItem1.Click +=newMenuItem1_Click;
                    _menu.Items.Add(newMenuItem1);

                    newMenuItem1.IsChecked = string.Equals( name,  ReadServerRegistryValue());
                }
            }
        }

        private void newMenuItem1_Click(object sender, RoutedEventArgs e)
        {
            // Uncheck all menu entry, except for the one the user has selected
            foreach( MenuItem mi in _menu.Items)
                mi.IsChecked = false;

            var item = (MenuItem)sender;
            item.IsChecked = true;

            WriteServerRegistryValue((string)item.Header);
        }

        private void WriteServerRegistryValue(string serverName)
        {
            var baseRegKey = ConfigurationManager.AppSettings["BaseRegKey"];
            var regKey = ConfigurationManager.AppSettings["RegKey"];

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
            using (var baseKey = hklm.OpenSubKey(baseRegKey,true))
            {
                if (baseKey == null)
                {
                    using(var newKey = hklm.CreateSubKey(baseRegKey, RegistryKeyPermissionCheck.ReadWriteSubTree))
                        newKey.SetValue(regKey, serverName);
                }
                else
                {
                    baseKey.SetValue(regKey, serverName);
                }
            }
        }

        private string ReadServerRegistryValue()
        {
            var baseRegKey = ConfigurationManager.AppSettings["BaseRegKey"];
            var regKey = ConfigurationManager.AppSettings["RegKey"];
            string server = "No Server Specified";

             using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
             using (var key = hklm.OpenSubKey(baseRegKey))
             {
                 if (key != null)
                     server = (string)key.GetValue(regKey, null);
             }
            return server;
        }

        private void MyNotifyIcon_PreviewTrayToolTipOpen(object sender, RoutedEventArgs e)
        {
            MyNotifyIcon.ToolTipText = ReadServerRegistryValue();
        }
    }
}