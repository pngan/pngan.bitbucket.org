﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace WebApplication1.Models
{
    public class AccessParameters
    {
        public AccessParameters(int daysOfTrial, int daysOfGrace)
        {
            DaysOfTrial = daysOfTrial;
            DaysOfGrace = daysOfGrace;
        }
        public int DaysOfTrial { get; }
        public int DaysOfGrace { get; }
    }
}