﻿using System;
using Enghouse;

namespace TestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            var columnizer = new EnghouseLogfileColumnizer();
            var input = "12:34:56.01 [DISP] QControl:this is a log: message";

            var columns = columnizer.SplitLine(null, input);
            var columnNames = columnizer.GetColumnNames();
            for (int i = 0; i < columnizer.GetColumnCount(); i++)
            {
                Console.WriteLine("Column '{0}' = {1}", columnNames[i], columns[i]);
            }
            Console.WriteLine();
            Console.WriteLine(columnizer.GetTimestamp(null, input));

            columnizer.SetTimeOffset(1000*60);
            Console.WriteLine(columnizer.GetTimestamp(null, input));

            Console.ReadLine();
        }
    }
}