﻿using System;
using System.Globalization;
using LogExpert;

namespace Enghouse
{
    public class EnghouseLogfileColumnizer : ILogLineColumnizer
    {
        private int m_msecOffset = 0;

        public string GetName()
        {
            return "Enghouse Columnizer";
        }

        public string GetDescription()
        {
            return "Columnizes entries in Enghouse log files";
        }

        public int GetColumnCount()
        {
            return 4;
        }

        public string[] GetColumnNames()
        {
            return new string[] { "Timestamp", "Thread", "Group", "Message" };

        }

        // 12:34:56.01 [DISP] QControl:this is a log: message
        // 0123456789012345678901234567
        public string[] SplitLine(ILogLineColumnizerCallback callback, string line)
        {
            if (string.IsNullOrEmpty(line))
                return new string[0];

            var timeStamp = line.Substring(0, 11);
            var thread = line.Substring(13, 4);

            var indexGroupEnd = line.IndexOf(":", 19, StringComparison.OrdinalIgnoreCase);
            var group = line.Substring(19, indexGroupEnd - 19);

            var message = line.Substring(indexGroupEnd+1);

            return new [] {timeStamp, thread, group, message};
        }

        public bool IsTimeshiftImplemented()
        {
            return true;
        }

        public void SetTimeOffset(int msecOffset)
        {
            m_msecOffset = msecOffset;
        }

        public int GetTimeOffset()
        {
            return m_msecOffset;
        }

        public DateTime GetTimestamp(ILogLineColumnizerCallback callback, string line)
        {
            if (line.Length < 11)
            {
                return DateTime.MinValue;
            }

            String s = line.Substring(0, 11);

            // Parse into a DateTime
            DateTime dateTime;
            if (!DateTime.TryParseExact(s, "HH:mm:ss.ff", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime))
                return DateTime.MinValue;

            // Add the time offset before returning
            return dateTime.AddMilliseconds(m_msecOffset);
        }

        public void PushValue(ILogLineColumnizerCallback callback, int column, string value, string oldValue)
        {
            throw new NotImplementedException();
        }
    }
}