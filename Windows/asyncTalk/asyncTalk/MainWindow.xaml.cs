﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace asyncTalk
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void StartCounterAsync()
        {
            await Task.Yield();
            for (int i = 0; i <= 120; i++)
            {
                UiCounter.Text = $"{i}";
                await Task.Delay(50);
            }

        }

        private void Button1_OnClick(object sender, RoutedEventArgs e)
        {
            UiOutput.Text = "Sync\n";
            StartCounterAsync();
            ServeBreakfast();
        }

        private void ServeBreakfast()
        {
            var order = GetOrder();
            var recipe = GetRecipe(order);
            var ingredients = GetIngredients(order);
            var meal = PrepareMeal(recipe, ingredients);
            ServeMeal(meal);
        }


        private void ServeMeal(object meal) => OutputProgress("ServeMeal");
        private object PrepareMeal(object recipe, object ingredients) => OutputProgress("PrepareMeal");
        private object GetIngredients(object recipe) => OutputProgress("GetIngredients");
        private object GetRecipe(object order) => OutputProgress("GetRecipe");
        private object GetOrder() => OutputProgress("GetOrder");
        private object OutputProgress(string output)
        {
            UiOutput.Text += output;
            Thread.Sleep(1000);
            UiOutput.Text += "... DONE\n";
            return null;
        }

        private void Button2_OnClick(object sender, RoutedEventArgs e)
        {
            UiOutput.Text = "Worker Thread\n";
            StartCounterAsync();
            try
            {
                Task.Run(() => ServeBreakfast());
            }
            catch
            {
                UiOutput.Text += "Exception\n";
            }
        }

        private async void Button3_OnClick(object sender, RoutedEventArgs e)
        {
            UiOutput.Text = "Awaited Worker Thread\n";
            StartCounterAsync();
            Task t = Task.CompletedTask;
            try
            {
                t = Task.Run(() => ServeBreakfast());
                await t;
            }
            catch
            {
                UiOutput.Text += $"Exception!! {t.Exception.InnerException.Message}\n";
            }
        }

        private async void Button5_OnClick(object sender, RoutedEventArgs e)
        {
            UiOutput.Text = "Await\n";
            StartCounterAsync();
            await ServeBreakfastAsync();
        }
        private async void Button6_OnClick(object sender, RoutedEventArgs e)
        {
            UiOutput.Text = "With await - parallel execution\n";
            StartCounterAsync();
            await ServeBreakfastFastAsync();
        }

        private async Task ServeBreakfastAsync()
        {
            var order = await GetOrderAsync();
            var recipe = await GetRecipeAsync(order);
            var ingredients = await GetIngredientsAsync(order);
            var meal = await PrepareMealAsync(recipe, ingredients);
            await ServeMealAsync(meal);
        }
        private async Task ServeBreakfastFastAsync()
        {
            var order = await GetOrderAsync();

            var t =  Task.WhenAll(GetRecipeAsync(order), GetIngredientsAsync(order));
            await t;

            var meal = await PrepareMealAsync(t.Result[0], t.Result[1]);
            await ServeMealAsync(meal);
        }
        private async Task ServeMealAsync(object meal) => await OutputProgressAsync("ServeMeal");
        private async Task<object> PrepareMealAsync(object recipe, object ingredients) => await OutputProgressAsync("PrepareMeal");
        private async Task<object> GetIngredientsAsync(object recipe) => await OutputProgressAsync("GetIngredients");
        private async Task<object> GetRecipeAsync(object order) => await OutputProgressAsync("GetRecipe");
        private async Task<object> GetOrderAsync() => await OutputProgressAsync("GetOrder");

        private async Task<object> OutputProgressAsync(string output)
        {
            await Task.Delay(600);
            UiOutput.Text += output;
            await Task.Delay(600);
            UiOutput.Text += "... DONE\n";
            return Task.CompletedTask;
        }

        private async void Button7_OnClick(object sender, RoutedEventArgs e)
        {
            UiOutput.Text = "Deadlock with Solutions\n";
            StartCounterAsync();
            var i = Async().Result; // BAD
            //var i = await Async(); // OK - method completes and allows Async() to complete
            //var i = Task.Run(Async).Result; // OK - Async continuation posted to ThreadPool thread
        }

        private async Task<int> Async()
        {
            await Task.Delay(20);
            return 0; // Code here has been posted back to the calling thread
        }


    }
}
