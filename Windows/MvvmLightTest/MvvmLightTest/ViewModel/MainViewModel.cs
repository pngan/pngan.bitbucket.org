﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.ConcurrencyVisualizer.Instrumentation;
using MvvmLightTest.Model;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MvvmLightTest.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        private string _welcomeTitle = string.Empty;
        private ObservableCollection<IItemViewModel> _demoItems = new ObservableCollection<IItemViewModel>();
        private CollectionView _demoItemsView;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                if (_welcomeTitle == value)
                {
                    return;
                }

                _welcomeTitle = value;
                RaisePropertyChanged("WelcomeTitle");
            }
        }

        public CollectionView DemoItemsView
        {
            get
            {
                return _demoItemsView;
            }

            set
            {
                if (_demoItemsView == value)
                {
                    return;
                }

                _demoItemsView = value;
                RaisePropertyChanged("DemoItemsView");
            }
        }


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService, IEnumerable<IItemViewModel> ViewModels)
        {
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    WelcomeTitle = item.Title;
                });
            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainThread";
            }

            _demoItemsView = (CollectionView)CollectionViewSource.GetDefaultView(_demoItems);

            foreach( var vm in ViewModels)
            {
                _demoItems.Add(vm);
            }

            _demoItemsView.MoveCurrentToFirst();
        }
    }
}