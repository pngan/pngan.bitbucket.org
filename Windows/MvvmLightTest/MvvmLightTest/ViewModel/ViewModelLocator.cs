﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:MvvmLightTest.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using MvvmLightTest.Model;
using Autofac;
using Autofac.Extras.CommonServiceLocator;

namespace MvvmLightTest.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            var container = new Autofac.ContainerBuilder();

            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container.Build()));

            if (ViewModelBase.IsInDesignModeStatic)
            {
                container.RegisterType<Design.DesignDataService>().As<IDataService>();
            }
            else
            {
                container.RegisterType<DataService>().As<IDataService>();
                container.RegisterType<ProgressViewModel>().As<IItemViewModel>();
                container.RegisterType<SpeechRecognitionViewModel>().As<IItemViewModel>();
            }

            container.RegisterType<MainViewModel>();
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }
    }
}