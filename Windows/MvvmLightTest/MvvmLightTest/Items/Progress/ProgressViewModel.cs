﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.ConcurrencyVisualizer.Instrumentation;
using MvvmLightTest.Model;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MvvmLightTest.ViewModel
{
    public interface IProgressViewModel : IItemViewModel
    { }

    public class ProgressViewModel : ViewModelBase, IProgressViewModel
    {
        private double _progressValue = 0.0;
        private CancellationTokenSource _cancellationTokenSource;
        private IProgress<double> _progress;

        private Span _executeSpan;

        public RelayCommand StartCommand { get; private set; }
        public RelayCommand StopCommand { get; private set; }
        public double ProgressValue 
        {
            get { return _progressValue; }
            set
            {
                if (_progressValue == value)
                {
                    return;
                }

                _progressValue = value;
                RaisePropertyChanged("ProgressValue");
            }
        }

        public string ItemName
        {
            get { return "Progress Demo"; }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public ProgressViewModel()
        {
            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainThread";
            }
            StartCommand = new RelayCommand(ExecuteStart);
            StopCommand = new RelayCommand(ExecuteStop);
            Markers.CreateMarkerSeries("**Progress Series");
            Markers.WriteMessage("Constructor");
        }

        private async void ExecuteStart()
        {
            if ( _executeSpan!=null)
            {
                _executeSpan.Leave();
            }

            if (_cancellationTokenSource != null)
                return;             // Already in progress
            _executeSpan = Markers.EnterSpan("Start");
            _cancellationTokenSource = new CancellationTokenSource();
            _progress = new Progress<double>(ReportProgress);
            await CountUpTask();
        }

        private void ReportProgress(double progress)
        {
            ProgressValue = progress;
        }

        private void ExecuteStop()
        {
            if (_executeSpan != null)
            {
                _executeSpan.Leave();
                _executeSpan = null;
            }
            Markers.WriteAlert("Stopped");
            if (_cancellationTokenSource != null)
                _cancellationTokenSource.Cancel();
        }

        private async Task CountUpTask()
        {
            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "CountUpSync";
            }
            Markers.WriteMessage("A message");
            double progressValue = ProgressValue;
            while (progressValue < 100.0)
            {
                if (_cancellationTokenSource.IsCancellationRequested)
                {
                    _cancellationTokenSource.Dispose();
                    _cancellationTokenSource = null;
                    break;
                }
                progressValue++;
                _progress.Report(progressValue);
                var span = Markers.EnterSpan("Delay");
                Markers.WriteFlag("**Delay");
                await Task.Delay(200);
                span.Leave();
            }
        }
    }
}
