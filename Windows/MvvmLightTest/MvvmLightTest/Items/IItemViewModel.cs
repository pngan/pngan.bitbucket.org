﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmLightTest.ViewModel
{
    public interface IItemViewModel
    {
        string ItemName { get; }
    }
}
