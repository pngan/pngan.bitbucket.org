﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.ConcurrencyVisualizer.Instrumentation;
using MvvmLightTest.Model;
using System;
using System.Speech.Recognition;
using System.Threading;
using System.Threading.Tasks;

namespace MvvmLightTest.ViewModel
{
    public interface ISpeechRecognitionViewModel : IItemViewModel
    { }

    public class SpeechRecognitionViewModel : ViewModelBase, ISpeechRecognitionViewModel
    {
        private string _speechOutput;
        private SpeechRecognitionEngine _recEngine = new SpeechRecognitionEngine();
        public string SpeechOutput 
        {
            get { return _speechOutput; }
            set
            {
                if (_speechOutput == value)
                {
                    return;
                }

                _speechOutput = value;
                RaisePropertyChanged("SpeechOutput");
            }
        }

        public RelayCommand InitializeCommand { get; private set; }

        public string ItemName
        {
            get { return "Speech Recognition"; }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public SpeechRecognitionViewModel()
        {
            InitializeCommand = new RelayCommand(Initialize);
        }

        private void Initialize()
        {
            SpeechOutput = "Say 'dial number', 'cancel call'";
            var choices = new Choices();
            choices.Add( new [] {"dial number", "cancel call"});
            var grammarBuilder = new GrammarBuilder();
            grammarBuilder.Append(choices);
            var grammar = new Grammar(grammarBuilder);

            _recEngine.LoadGrammar(grammar);
            _recEngine.SetInputToDefaultAudioDevice();
            _recEngine.SpeechRecognized += recEngine_SpeechRecognized;
            _recEngine.RecognizeCompleted += _recEngine_RecognizeCompleted;
            _recEngine.RecognizeAsync();
        }

        void _recEngine_RecognizeCompleted(object sender, RecognizeCompletedEventArgs e)
        {
            _recEngine.RecognizeAsync();
        }

        void recEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            switch (e.Result.Text)
            {
                case "dial number":
                    SpeechOutput += "\nDial Number";
                    break;
                case "cancel call":
                    SpeechOutput += "\ncancel call";
                    break;
                default:
                break;
            }
        }
    }
}
