﻿using System;
using MvvmLightTest.Model;

namespace MvvmLightTest.Design
{
    public class DesignDataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Lite [design]");
            callback(item, null);
        }
    }
}