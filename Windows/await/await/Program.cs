﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace await
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main start");
            Task t = ConsoleAsyncHost();


            t.Wait();
        }

        public async static Task ConsoleAsyncHost()
        {
            var primes = await RunAsync<IList<int>>(() => findPrimes(10000000));
            Console.WriteLine("Largest prime = {0}", primes[primes.Count-1]);
        }

        public async static Task<string> asyncMethod()
        {
            Console.WriteLine("public async static  void asyncMethod() start");
            await Task.Delay(100);
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("public async static  void asyncMethod() end");
            return "finished";
        }


        static IList<int> findPrimes(int max) 
        {
            var vals = new List<int>((int)(max/(Math.Log(max)-1.08366)));
            var maxSquareRoot = Math.Sqrt(max);
            var eliminated = new System.Collections.BitArray(max + 1);                        

            vals.Add(2);

            for (int i = 3; i <= max; i+=2) {
                if (!eliminated[i]) {
                    if (i < maxSquareRoot) {
                        for (int j = i * i; j <= max; j+=2*i)
                            eliminated[j] = true;
                    }
                    vals.Add(i);
                }
            }
            return vals;
        }


        public static Task<T> RunAsync<T>(Func<T> function) 
        { 
            if (function == null) throw new ArgumentNullException("function"); 
            var tcs = new TaskCompletionSource<T>(); 
            ThreadPool.QueueUserWorkItem(_ => 
            { 
                try 
                {  
                    T result = function(); 
                    tcs.SetResult(result);  
                } 
                catch(Exception exc) 
                { 
                    tcs.SetException(exc); 
                } 
            }); 
            return tcs.Task; 
        }
    }
}
