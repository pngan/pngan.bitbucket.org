﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DemoConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DemoAsync().Wait();   
        }

        static async Task DemoAsync()
        {
            var dict = new Dictionary<int,int>();
            for (int i = 0; i < 10000; i++)
            {
                var id = Thread.CurrentThread.ManagedThreadId;
                int count;
                if (dict.TryGetValue(id, out count))
                    dict[id] = count + 1;
                else
                    dict[id] = 1;
                await Task.Yield();

            }
            foreach (var pair in dict)
            {
                Console.WriteLine(pair);
            }
            Console.ReadLine();
        }
    }
}
