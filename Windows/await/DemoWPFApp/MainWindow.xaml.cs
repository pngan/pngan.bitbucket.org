﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DemoWPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
           // await DemoAsync();
            //await DemoAsync2();
            await DemoAsync3();
        }

        static async Task  DemoAsync2()
        {
            var tasks = new List<Task<int>>
            {
                DelayTask(1, 1000),
                DelayTask(2, 4000)
            };

            //Task.WhenAll(tasks);

            foreach (var task in tasks)
            {
                var result = await task;
                Console.WriteLine("{0} Completed loop task {1} in thread '{2}'", DateTime.Now, result, Thread.CurrentThread.ManagedThreadId);
            }

            Console.WriteLine("All tasks started.");
        }

        static async Task DemoAsync3()
        {
            for (int i = 0; i < 3; i++)
            {
                await Task.Delay(1000);
                Console.WriteLine("{0} Completed loop task in thread '{1}'", DateTime.Now, Thread.CurrentThread.ManagedThreadId);
            }
        }


        static async Task<int> DelayTask(int tag, int delayMs)
        {
            //var tcs = new TaskCompletionSource<int>();
            Console.WriteLine("{0} Starting task {1} in thread '{2}'", DateTime.Now, tag, Thread.CurrentThread.ManagedThreadId);
            await Task.Delay(delayMs);
            Console.WriteLine("{0} Completed task {1} in thread '{2}'", DateTime.Now, tag, Thread.CurrentThread.ManagedThreadId);
            return tag;
        }

        static async Task DemoAsync()
        {
            var dict = new Dictionary<int, int>();
            for (int i = 0; i < 10000; i++)
            {
                var id = Thread.CurrentThread.ManagedThreadId;
                int count;
                if (dict.TryGetValue(id, out count))
                    dict[id] = count + 1;
                else
                    dict[id] = 1;
                await Task.Yield();

            }
            foreach (var pair in dict)
            {
                Console.WriteLine(pair);
            }
            Console.ReadLine();
        }
    }
}
