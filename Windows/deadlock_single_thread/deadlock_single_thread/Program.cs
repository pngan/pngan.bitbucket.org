﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deadlock_single_thread
{
    class DeadlockClass
    {
        
        object fooLock = new Object();
        object barLock = new Object();

        public void foo()
        {
            lock(fooLock)
            {
                bar();
            }
        }

        public void bar()
        {
            lock(barLock)
            {
                var t = Task.Factory.StartNew(() => foo());
                t.Wait();
            }
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            var deadlockClass = new DeadlockClass();
            deadlockClass.foo();
        }
    }
}
