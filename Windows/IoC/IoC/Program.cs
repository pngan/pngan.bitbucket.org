﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC
{
    public interface IStringDecorator
    {
        string DecorateString(string input);
    }

    public class EmphasizeText : IStringDecorator
    {
        public string DecorateString(string input)
        {
            return string.Format("**{0}**", input);
        }
    }

    public interface IPrintable
    {
        void PrintName();
    }

    public class TestClass : IPrintable
    {
        private IStringDecorator m_decorator;
        public TestClass( IStringDecorator decorator)
        {
            m_decorator = decorator;
        }
        public void PrintName()
        {
            Console.WriteLine(m_decorator.DecorateString("TestClass"));
        }
    }

    public class Container
    {
        private Dictionary<Type, Type> m_typeMap = new Dictionary<Type, Type>();
        public IPrintable Create<T>()
        {
            return Create(typeof(T));
        }

        private IPrintable Create(Type t)
        {
            Type resultType = m_typeMap[t];
            return (IPrintable)Activator.CreateInstance(resultType, Create(typeof(IStringDecorator)));
        }

        internal void Register<T>(Type type)
        {
            m_typeMap[type] = typeof(T);
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var container = new Container();
            container.Register<EmphasizeText>(typeof(IStringDecorator));
            container.Register<TestClass>(typeof(IPrintable));
            var obj = container.Create<IPrintable>();
            obj.PrintName();
            Console.ReadLine();
        }
    }
}
