﻿using System;
using System.Windows;
using Autofac;
using Model;
using ViewModel;
using System.Diagnostics;

namespace WPFStarter
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            var builder = new ContainerBuilder();
            RegisterDependencies(builder);
            var container = builder.Build();

            StartApplication(container);
        }

        private void RegisterDependencies(ContainerBuilder builder)
        {
            builder.RegisterType<MainModel>().As<IMainModel>();
            builder.RegisterType<MainViewModel>().As<IMainViewModel>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<SubWorker>().As<ISubWorker>(); // This will create a new object every time
            //builder.RegisterType<SubWorker>().As<ISubWorker>().InstancePerLifetimeScope(); // This will create a single instance per Owned root object
            builder.RegisterType<View.MainWindow>();
        }

        private static void StartApplication(IContainer container)
        {
            var window = container.Resolve<View.MainWindow>();
            window.Show();
        }
    }

}