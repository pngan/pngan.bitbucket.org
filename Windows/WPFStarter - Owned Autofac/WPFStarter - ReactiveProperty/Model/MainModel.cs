﻿using System;
using System.Reactive.Linq;

namespace Model
{
    public class MainModel : IMainModel
    {

        public void Start()
        {
            
        }

        public void SetName(string name)
        {
            OnGreetingChanged?.Invoke(this, new GreetingArgs(name));
        }

        public event EventHandler<GreetingArgs> OnGreetingChanged;

        public IObservable<GreetingArgs> GreetingObservable
        {
            get
            {
                return Observable
                    .FromEventPattern<GreetingArgs>(
                        h => this.OnGreetingChanged += h,
                        h => this.OnGreetingChanged -= h)
                    .Select(x => x.EventArgs);
            }
        }
    }
}