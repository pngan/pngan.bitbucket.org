﻿using System;
using System.Diagnostics;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Model;
using Reactive.Bindings;
using Autofac;
using Autofac.Features.OwnedInstances;

namespace ViewModel
{
    public interface IMainViewModel
    {
        void Start();
    }

    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private readonly IMainModel m_model;
        private readonly Owned<IUnitOfWork> m_unitOfWork;

        public MainViewModel(IMainModel model, Owned<IUnitOfWork> unitOfWork)
        {
            m_model = model;
            m_unitOfWork = unitOfWork;
            Greeting = model.GreetingObservable.Select(x => x.Greeting).ToReadOnlyReactiveProperty();
            Name.Subscribe(m_model.SetName);
        }

        public ReactiveProperty<string> Name { get; set; } = new ReactiveProperty<string>();

        public ReadOnlyReactiveProperty<string> Greeting { get; }

        
        public void Start()
        {
            m_model.Start();
            m_unitOfWork.Value.DoWork();
            m_unitOfWork.Dispose(); // This will cause the Dispose() method to be called on all SubWorkers
        }
    }

    public interface IUnitOfWork
    {
        void DoWork();
    }
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISubWorker m_subworker1;
        private readonly ISubWorker m_subworker2;

        public UnitOfWork(ISubWorker subworker1, ISubWorker subworker2)
        {
            m_subworker1 = subworker1;
            m_subworker2 = subworker2;
        }
        public void DoWork()
        {
            m_subworker1.DoSubTask();
            m_subworker2.DoSubTask();
        }
    }

    public interface ISubWorker
    {
        void DoSubTask();
    }

    public class SubWorker : ISubWorker, IDisposable
    {
        private string m_guid;

        public SubWorker()
        {
            m_guid = Guid.NewGuid().ToString().Substring(0, 5);
            Debug.WriteLine($"Creating SubWorker {m_guid}");
        }
        public void Dispose()
        {
            Debug.WriteLine($"Disposing SubWorker {m_guid}");
        }

        public void DoSubTask()
        {
            Debug.WriteLine($"Executing SubTask() {m_guid}");
        }
    }
}