﻿using System;

namespace Model
{
    public interface IMainModel
    {
        void Start();
        void GetContact(string firstName);
        event EventHandler<ContactListArgs> OnContactChanged;
        IObservable<ContactListArgs> ResetContactsObservable { get;  }
    }
}