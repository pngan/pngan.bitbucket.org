using System;
using System.Collections;
using System.Collections.Generic;

namespace Model
{
    public class GreetingArgs : EventArgs
    {
        private readonly string m_name;

        public GreetingArgs(string name)
        {
            m_name = name;
        }

        public string Greeting => $"Hello, {m_name}!";
    }
    public class Contact : EventArgs
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set;  }

        //public Contact(string firstName, string lastName, string email)
        //{
        //    FirstName = firstName;
        //    LastName = lastName;
        //    Email = email;
        //}
    }

    public class ContactListArgs : EventArgs
    {
        public IEnumerable<Contact> Contacts { get; set; }


        public ContactListArgs(IEnumerable<Contact> contacts)
        {
            Contacts = contacts;
        }
        
    }

}