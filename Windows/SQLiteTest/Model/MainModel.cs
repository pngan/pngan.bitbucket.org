﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reactive.Linq;
using Dapper;
using Microsoft.Data.Sqlite;

namespace Model
{
    public class MainModel : IMainModel
    {
        private SqliteConnection m_dbConn;

        private SqliteConnection DbConn
        {
            get { return m_dbConn; }
            set { m_dbConn = value; }
        }

        public void Start()
        {
            DbConn = SqLiteBaseRepository.SimpleDbConnection();
            DbConn.Open();
        }

        public void GetContact(string firstName)
        {
            var results = DbConn.Query<Contact>("SELECT * FROM Names WHERE FirstName like @FirstName", new {FirstName = firstName+"%"});
            OnContactChanged?.Invoke(this, new ContactListArgs(results));
        }

        public event EventHandler<ContactListArgs> OnContactChanged;

        public IObservable<ContactListArgs> ResetContactsObservable
        {
            get
            {
                return Observable
                    .FromEventPattern<ContactListArgs>(
                        h => this.OnContactChanged += h,
                        h => this.OnContactChanged -= h)
                    .Select(x => x.EventArgs);
            }
        }
        
    }

    public interface IContactRepository
    {
        ContactRecord GetContact(string firstName);
    }

    public class SqLiteBaseRepository
    {
        public static string DbFile
        {
            get { return Environment.CurrentDirectory + "\\phonenumber.sqlite3"; }
        }

        public static SqliteConnection SimpleDbConnection()
        {
            return new SqliteConnection("Data Source=" + DbFile);
        }
    }
    public class ContactRecord
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }


}