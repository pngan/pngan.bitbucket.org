﻿using System;
using System.Reactive.Linq;
using System.Windows.Input;
using Model;
using Reactive.Bindings;

namespace ViewModel
{
    public interface IMainViewModel
    {
        void Start();
    }

    public class MainViewModel :  ViewModelBase, IMainViewModel
    {
        private readonly IMainModel m_model;
        private string m_name;

        public MainViewModel(IMainModel model)
        {
            m_model = model;

            Contacts = model
                .ResetContactsObservable
                .Do(c=>Contacts.Clear())
                .SelectMany(c=>c.Contacts)
                .ToReactiveCollection();

            PerformSearchCommand = new DelegateCommand(o =>
            {
                m_model.GetContact(Name);
            }
            );
        }

        public ReactiveCollection<Contact> Contacts { get; set; }

        public string Name
        {
            get { return m_name; }
            set
            {
                m_name = value;
                OnPropertyChanged("Name");
            }
        }

        public ICommand PerformSearchCommand { get; }

        public void Start()
        {
            m_model.Start();
        }
    }

    public class DelegateCommand : ICommand
    {
        private readonly Predicate<object> m_canExecute;
        private readonly Action<object> m_execute;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action<object> execute)
                       : this(execute, null)
        {
        }

        public DelegateCommand(Action<object> execute,
                       Predicate<object> canExecute)
        {
            m_execute = execute;
            m_canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (m_canExecute == null)
            {
                return true;
            }

            return m_canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            m_execute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }
    }
}