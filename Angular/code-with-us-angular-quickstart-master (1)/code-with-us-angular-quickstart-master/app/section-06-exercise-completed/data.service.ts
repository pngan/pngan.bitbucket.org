import { Http, Headers } from '@angular/http';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { Customer } from './model';
import { LoggerService } from './logger.service';
import { Injectable } from '@angular/core';
import { createTestCustomers } from './test-data';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class DataService {
    private customersUrl = 'api/customers';
    private statesUrl = 'api/states';

    constructor(
        private loggerService: LoggerService,
        private http: Http) {

    }

    getCustomersP(): Promise<Customer[]> {
        this.loggerService.log(`Getting customers as a promise via http ...`);

        return this.http.get(this.customersUrl)
        .toPromise()
        .then(response =>  {
            const custs = response.json().data as Customer[];
            this.loggerService.log(`Got ${custs.length} customers.`);
            return custs;
        },
        error => {
                this.loggerService.log(`Error occurred : ${error}`);
                return Promise.reject('Something bad happened please check the console');
        }
        );
    }
    getCustomers(): Observable<Customer[]> {
        this.loggerService.log(`Getting customers as an observable via Http ...`);
        return this.http.get(this.customersUrl)
            .map(response => response.json().data as Customer[])
            .do((custs) => {
                this.loggerService.log(`Got ${custs.length} customers.`);
            })
            .catch((error:any) => {
                this.loggerService.log(`Error occurred : ${error}`);
                    return Observable.throw('Something bad happened getting customers please check the console');
            });
    }
    getStates(): Observable<string[]> {
        this.loggerService.log(`Getting states as an observable via Http ...`);
        return this.http.get(this.statesUrl)
            .map(response => response.json().data as string[])
            .do((st) => {
                this.loggerService.log(`Got ${st.length} states.`);
            })
            .catch((error:any) => {
                this.loggerService.log(`Error occurred : ${error}`);
                    return Observable.throw('Something bad happened getting states please check the console');
            });
    }
}
