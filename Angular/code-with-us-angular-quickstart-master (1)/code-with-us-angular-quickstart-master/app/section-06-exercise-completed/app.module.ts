import { LoggerService } from './logger.service';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }   from '@angular/http';


import { AppComponent }  from './app.component';
import { CustomerListComponent } from './customer-list.component';
import { CustomerDetailComponent } from './customer-detail.component';
import { AddressComponent } from './address.component';
import { DataService } from './data.service';

// todo remove for production
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

@NgModule({
  imports:      [                  // What stuff do I need?
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService)
  ],
  declarations: [ AppComponent,
  CustomerListComponent,
  CustomerDetailComponent,
  AddressComponent ],  // What's in my app module?
  providers: [DataService, LoggerService],
  bootstrap:    [ AppComponent ]   // Where do I start?
})
export class AppModule { }
