import { Component, Input, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { LoggerService } from './logger.service';
import { Address } from './model';

@Component({
  moduleId: module.id,
  selector: 'address-comp',
  templateUrl: 'address.component.html'
})

export class AddressComponent implements OnInit {

    isBusy = false;

  constructor(
    private dataService: DataService,
    private loggerService: LoggerService) { }

  ngOnInit(): void {
    this.getStates();
  }

  getStates() {
    this.isBusy = true;
    this.loggerService.log('Getting states ...');
    this.dataService.getStates().subscribe(st => {
      this.isBusy = false;
      this.states = st;
    }, (errorMsg: string) => {
      this.isBusy = false;
      alert(errorMsg);
    });
  }
  @Input() address: Address;

  regions   = ['East', 'Midwest', 'North', 'South', 'West'];

  /* Create an array of states that includes previous states PLUS Illinois */
  states: string[]   = [];
}
